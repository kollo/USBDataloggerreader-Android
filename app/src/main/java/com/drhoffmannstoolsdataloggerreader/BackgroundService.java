package com.drhoffmannstoolsdataloggerreader;

/* BackgroundService.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 



import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Locale;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;



public class BackgroundService extends Service  {
  private final int UPDATE_INTERVAL = 60*1000;
  private static final String TAG = "USBDL-bg";
  private NotificationManager mNotificationManager;
  Notification mNotification;
  static Logger mlogger=null;
  private Timer timer = new Timer();
  private static final int NOTIFICATION_ID = 4711;
  int count=100;
  static int interval=-1;
  static String destination="";
  boolean getdataenabled=false;
  boolean savedataenabled=false;

  @Override
  public IBinder onBind(Intent arg0)           { return null;     }
  public static void set_logger(Logger logger) { mlogger=logger;  }
  public static void set_interval(int min)     { interval=min;    }
  public static void set_uri(String uri)       { destination=uri; }
  @Override
  public void onCreate() {
    mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
  }

	@Override
	public int onStartCommand(Intent intent, int flags, int startid) {
		//	String action = intent.getAction();
		//   if (action.equals("")) processPlayRequest();
		//   else if (action.equals("")) processPauseRequest();


		int icon = android.R.drawable.ic_dialog_info;
		CharSequence tickerText = "Service started...";
		long when = System.currentTimeMillis();
		mNotification = new Notification(icon, tickerText, when);
		Context context = getApplicationContext();
		CharSequence contentTitle = "USBDataloggerreader";
		CharSequence contentText = "TIMER OK!";
		count=interval;
		Intent notificationIntent = new Intent(this, USBDataloggerreaderActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,notificationIntent, 0);
		mNotification.setLatestEventInfo(context, contentTitle, contentText,contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mNotification);
		Log.d("TAG","Service started.");
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				// Check if there are updates here and notify if true
				if(mlogger==null) updateNotification("no logger connected, TIMER: "+count+" Minutes.",android.R.drawable.presence_offline);
				else if(interval<=0) updateNotification("no TIMER set!",android.R.drawable.presence_offline);
				else if(count<=0) {
					getdataenabled=false;
					if(mlogger!=null && mlogger.isconnected) { /* Überprüfe, ob noch ein Logger angeschlossen ist, */
						mlogger.be_quiet=true;
						getconfig();          /* wenn ja, mache kontaktieren.*/
						if(getdataenabled) {
							getdata();        /* dann get data */
						}
						if(savedataenabled) {
							/* dann speichern
							 * dann start messung.
							 * dann evtl. laden zum Server
							 * dann */
							String tag=mlogger.loggertype+"-"+mlogger.config.getname()+"-"+
									String.format(Locale.US,"%04d-%02d-%02d-%02d-%02d",mlogger.config.time_year,
											(int)(mlogger.config.time_mon&0xff),(int)(mlogger.config.time_mday&0xff),
											(int)(mlogger.config.time_hour&0xff),(int)(mlogger.config.time_min&0xff));
							String filename="AVDL"+tag+"."+"txt";
							Log.d(TAG,"Service save "+filename);

							LoggerData.save(mlogger,filename,"txt",false,"Automatically read out by USBDataloggerreader for Android.");
							if(mlogger!=null && mlogger.data.issaved) {
								/*Neue Messung starten*/
								sendConfig();

								// TODO: ftpUpload(String urls,  File source)
								File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

								File file=new File(dirdata,filename);
								String dest=String.format(destination, tag);
								if(file.exists() && !dest.isEmpty()) ftpUpload(dest, file);

							}


						}
						if(mlogger!=null && mlogger.isreadconfig) sendConfig();
						mlogger.be_quiet=false;
					} else updateNotification("ERROR: logger not connected!",android.R.drawable.ic_delete);
					count=interval;
				} else {
					updateNotification("TIMER set in "+count+" Minutes.",android.R.drawable.presence_online);
				}
				count--;
			}
		}, 0, UPDATE_INTERVAL);
		return START_STICKY;
	}



	/*Read configuration from device
	 * */
	private void getconfig() {
		Log.d(TAG,"Service get config...");
		updateNotificationFg("Get Config...",android.R.drawable.star_big_on);
		int ret=mlogger.get_config(); 
		final LoggerConfig config=mlogger.config;
		if(ret==Error.ERR)  updateNotification("ERROR: Get config failed. transmission timeout.",android.R.drawable.ic_delete);
		else if(ret==Error.ERR_BAD) updateNotification("ERROR: Config BAD.",android.R.drawable.ic_delete);
		else if(ret==Error.OK || ret==Error.WARN_NOTSUP || ret==Error.WARN_NOTTESTED|| ret>0) updateNotification(mlogger.get_status(),android.R.drawable.stat_sys_warning);
		if(ret==Error.WARN_NOTSUP) ;  /*Notsupported/Not tested Message*/ 
		else if(ret==Error.WARN_NOTTESTED) ;  /*Notsupported/Not tested Message*/ 

		if((config.flag_bits&Lascar.SENSOR_FAIL)==Lascar.SENSOR_FAIL) config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_sensorerror);
		if((config.flag_bits&Lascar.BATTERY_FAIL)==Lascar.BATTERY_FAIL)   config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_batterycritical);
		else if((config.flag_bits&Lascar.BATTERY_LOW)==Lascar.BATTERY_LOW) config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_batterylow);

		if(config.num_data_rec>0) getdataenabled=true;

		if(config.mglobal_message.length()>0) updateNotification("Message: "+config.mglobal_message,android.R.drawable.star_big_on);
		savedataenabled=false;
	}	
	private void getdata() {
		final LoggerConfig config=mlogger.config;
		final LoggerData data=mlogger.data;
		Log.d(TAG,"Service get data..."+config.num_data_rec+" adr1="+String.format("%04x", config.config_begin));
		Log.d(TAG," adr2="+String.format("%04x", config.config_end));
		if(config.num_data_rec==0) {
			updateNotification("No Data !",android.R.drawable.stat_sys_warning);
			data.clear();
			return;
		}
		updateNotification("Get Data...",android.R.drawable.stat_notify_sync);
		if(mlogger.protocol==Logger.PROTO_HID) config.flag_bits2=1; /*sonst wird der ganze logger gelesen!*/
		mlogger.get_data();
		data.set_interval(config.got_interval);
		if(mlogger.loggertype==Logger.LTYP_TH || mlogger.loggertype==Logger.LTYP_THP) data.calc_events();
		savedataenabled=(data.anzdata>0);
		updateNotification("Got all Data.",android.R.drawable.ic_menu_agenda);
		if(data.quality>0) {
			updateNotification("Data: Quality Warning.",android.R.drawable.stat_sys_warning);
		}
	}	
	private void sendConfig() {
		Log.d(TAG,"Service send config...");
		if (mlogger.isconnected) {
			updateNotification("Send Config...",android.R.drawable.stat_notify_sync);
			/* When data has not been saved yet, display a warning !*/
			byte[] confmessagebuf = build_conf();
			if(confmessagebuf!=null) {
				/* Wenn alte config noch nicht gelesen war, warnung ausgeben*/
				if(!mlogger.isreadconfig) 
					Log.d(TAG,"Something is wrong..."+mlogger.isreadconfig+" "+mlogger.isreaddata);  /* Handle real request*/
				else realsendconfig(confmessagebuf);
			} else Log.d(TAG, "configuration problem ");
		} else Log.d(TAG, "connection problem ");
	}

	/*Konfiguriere den Logger für Messung/Datennahme*/

	private void realsendconfig(byte[] confmessagebuf) {
		Log.d(TAG,"Service real send config...");
		int ret=mlogger.send_config(confmessagebuf);
		if(ret==0) {
			updateNotification("Send Config done.",android.R.drawable.ic_menu_agenda);
		} else if(ret==Error.ERR) updateNotification("Send Config failed !",android.R.drawable.stat_sys_warning);
	}
	private byte[] build_conf() {
		configfrominput();
		int e=mlogger.chk_conf();
		if(e==Error.OK) return mlogger.build_conf();
		return null;
	}
	private void configfrominput() {
		final LoggerConfig config=mlogger.config;
		Date dt = new Date();
		config.time_year=dt.getYear()+1900;
		config.time_mon=(byte) (dt.getMonth()+1);
		config.time_mday=(byte)dt.getDate();
		config.time_hour=(byte)dt.getHours();
		config.time_min=(byte)dt.getMinutes();
		config.time_sec=(byte)dt.getSeconds();
		config.time_offset=0;
	}

	/** Updates the notification. */
	void updateNotification(String text,int icon) {
		Log.d(TAG,"Notification: "+text);
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
				new Intent(getApplicationContext(), USBDataloggerreaderActivity.class),
				PendingIntent.FLAG_UPDATE_CURRENT);
		mNotification.setLatestEventInfo(getApplicationContext(), "USBDatalogger", text, pi);
		mNotification.icon=icon;
		mNotification.flags |= Notification.FLAG_ONGOING_EVENT;
		mNotificationManager.notify(NOTIFICATION_ID, mNotification);
	} 
	/**
	 * Configures service as a foreground service. A foreground service is a service that's doing
	 * something the user is actively aware of (such as playing music), and must appear to the
	 * user as a notification. That's why we create the notification here.
	 */
	void updateNotificationFg(String text,int icon) {
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
				new Intent(getApplicationContext(), USBDataloggerreaderActivity.class),
				PendingIntent.FLAG_UPDATE_CURRENT);
		mNotification = new Notification();
		mNotification.tickerText = text;
		mNotification.icon = icon;
		mNotification.flags |= Notification.FLAG_ONGOING_EVENT;
		mNotification.setLatestEventInfo(getApplicationContext(), "USBDatalogger",text, pi);
		startForeground(NOTIFICATION_ID, mNotification);
	}


  /*Android Permission internet is needed.*/
  /*URL: ftp://user:passwd@server/filename;type=i   */

  void ftpUpload(String urls,  File source) {
    BufferedInputStream bis = null;
    BufferedOutputStream bos = null;
    Log.d(TAG,"Service Ftpupload to "+urls);
    try {
      URL url = new URL(urls);
      URLConnection urlc = url.openConnection();

      bos = new BufferedOutputStream( urlc.getOutputStream() );
      bis = new BufferedInputStream( new FileInputStream( source ) );

      int i;
      // read byte by byte until end of stream
      while ((i = bis.read()) != -1) bos.write(i);
    } 
    catch (MalformedURLException e) { e.printStackTrace(); }
    catch (IOException e) { e.printStackTrace(); }
    finally {
      if(bis != null)
        try { bis.close(); }
        catch (IOException ioe) { ioe.printStackTrace(); }
      if (bos != null)
        try { bos.close(); }
        catch (IOException ioe) { ioe.printStackTrace(); }
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    Toast.makeText(this, "USB Dataloggerreader service destroyed ...", Toast.LENGTH_LONG).show();
  }
}
