package com.drhoffmannstoolsdataloggerreader;

/* InfoActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;

public class InfoActivity extends Activity {
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.info);
    ActionBar actionBar = getActionBar();
    //  actionBar.setHomeButtonEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(false);
    Button fertig=(Button)findViewById(R.id.okbutton);
    TextView readme=(TextView)findViewById(R.id.description);
    readme.setText(Html.fromHtml(getResources().getString(R.string.description)+getResources().getString(R.string.readme)+
        	   getResources().getString(R.string.news)+getResources().getString(R.string.impressum)
        	   ));
    fertig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {finish();}
    });
  }
    

  @Override
  public boolean onOptionsItemSelected(final MenuItem item) {
    switch (item.getItemId())  {
    case android.R.id.home:
      // This is called when the Home (Up) button is pressed
      // in the Action Bar.
      finish();
      return true;
    default: 
      return super.onOptionsItemSelected(item);
    }
  }    
}
