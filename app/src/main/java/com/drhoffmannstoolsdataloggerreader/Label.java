package com.drhoffmannstoolsdataloggerreader;

/* Label.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2023
 * =======================================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

public class Label {
  String name;
  int idx;
}
