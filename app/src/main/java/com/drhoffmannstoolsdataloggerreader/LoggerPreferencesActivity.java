package com.drhoffmannstoolsdataloggerreader;

/* LoggerPreferencesActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 



import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.pm.PackageManager.NameNotFoundException;
import java.util.Locale;


public class LoggerPreferencesActivity  extends Activity {
  private static final String TAG = LoggerPreferencesActivity.class.getSimpleName(); 
  private CheckBox rawdata,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb,fc,fd,fe,ff,fg,fh,fi,fj,fk;
  private TextView vid,pid,id,protocol,ltyp,packet,memory,serial,actual,textactual,fversion;
  private TextView textsensor,sensor,textbattery,battery,textpath,path;
  private EditText cala,calb,unit,cal1,cal2,cal3,cal4,halm,lalm,defunit,defiunit,defcal1,defcal2,defcal3,defcal4;
  private EditText defhalm,deflalm,defrangedes;
  private EditText name,owner,report,location,comment,comment2,loggerid;
  private EditText pdffilename;
  private EditText tempoffset;
  private EditText humioffset;
  private Button language,dateformat,timeformat;
  private Button tempunit,presunit;
  private RadioGroup blocktype;
  private Button repair,recalibrate,emailconfig;
  private RadioButton b1,b2,b3,b4,b5,b6,b7,b0,b8,b9,b10,b11,b12,b13;
  private RelativeLayout relcal;
  private LinearLayout fgroup1,fgroup2,fgroup3,fgroup4,fgroup5;
  private LinearLayout medialoggergroup; 
  private LinearLayout timeformatgroup;
  private LinearLayout dateformatgroup;
  private LinearLayout tempunitgroup;
  private LinearLayout presunitgroup;
  private LinearLayout tempoffsetgroup;
  private LinearLayout humioffsetgroup;
  private static LoggerConfig config;
  private static Logger logger;
  private ListSelect languageselect=new ListSelect();
  private static final String[] languages={
  	  "german","english","french","italian","dutch"
  };
  private static final Integer[] languageidx={
    Logger.LANGUAGE_GERMAN,Logger.LANGUAGE_ENGLISH,Logger.LANGUAGE_FRENSH,
    Logger.LANGUAGE_ITALIAN,Logger.LANGUAGE_DUTCH};

  private ListSelect dateformatselect=new ListSelect();
  private static final String[] dateformats={"YYYYMMDD","DDMMYYYY","MMDDYYYY"};
  private static final Integer[] dateformatidx={
    Logger.DATEFORMAT_YYYYMMDD,Logger.DATEFORMAT_DDMMYYYY,Logger.DATEFORMAT_MMDDYYYY};

  private ListSelect timeformatselect=new ListSelect();
  private static final String[] timeformats={"12H","24H"};
  private static final Integer[] timeformatidx={Logger.TIMEFORMAT_12H,Logger.TIMEFORMAT_24H};

  private ListSelect tempunitselect=new ListSelect();
  private static final String[] tempunits={"°C","°F"};
  private static final Integer[] tempunitidx={Logger.UNIT_C,Logger.UNIT_F};

  private ListSelect presunitselect=new ListSelect();
  private static final String[] presunits={"hPa","mm Hg","kPa"};
  private static final Integer[] presunitidx={Logger.UNIT_HPA,Logger.UNIT_MMHG,Logger.UNIT_KPA};

  private int findin(Integer a[],int b) {
    for(int i=0;i<a.length;i++) {
      if(a[i]==b) return(i);
    }
    return(0);
  }



  SharedPreferences prefs;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.loggerprefs);
    ActionBar actionBar = getActionBar();
    if (actionBar != null) {
        //  actionBar.setHomeButtonEnabled(true);
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    actionBar.setTitle(R.string.title_loggersettings);
    }

    languageselect.setlist(languages);
    dateformatselect.setlist(dateformats);
    timeformatselect.setlist(timeformats);
    tempunitselect.setlist(tempunits);
    presunitselect.setlist(presunits);


    vid=(TextView)findViewById(R.id.vid);
    pid=(TextView)findViewById(R.id.pid);
    id=(TextView)findViewById(R.id.id);
    protocol=(TextView)findViewById(R.id.proto);
    ltyp=(TextView)findViewById(R.id.ltyp);
    packet=(TextView)findViewById(R.id.packet);
    memory=(TextView)findViewById(R.id.memory);
    name=(EditText)findViewById(R.id.name);
    loggerid=(EditText)findViewById(R.id.loggerid);
    owner=(EditText)findViewById(R.id.owner);
    location=(EditText)findViewById(R.id.location);
    report=(EditText)findViewById(R.id.report);
    comment=(EditText)findViewById(R.id.comment);
    comment2=(EditText)findViewById(R.id.comment2);
    pdffilename=(EditText)findViewById(R.id.pdffilename);
    language=(Button)findViewById(R.id.language);
    dateformat=(Button)findViewById(R.id.dateformat);
    timeformat=(Button)findViewById(R.id.timeformat);
    tempunit=(Button)findViewById(R.id.tempunit);
    presunit=(Button)findViewById(R.id.presunit);
    serial=(TextView)findViewById(R.id.serial);
    fversion=(TextView)findViewById(R.id.fversion);
    path=(TextView)findViewById(R.id.path);
    textpath=(TextView)findViewById(R.id.textpath);

    battery=(TextView)findViewById(R.id.battery);
    textbattery=(TextView)findViewById(R.id.textbattery);
    sensor=(TextView)findViewById(R.id.sensor);
    textsensor=(TextView)findViewById(R.id.textsensor);
    actual=(TextView)findViewById(R.id.actual);
    textactual=(TextView)findViewById(R.id.textactual);
    cala=(EditText) findViewById(R.id.cala);
    calb=(EditText) findViewById(R.id.calb);
    repair=(Button)findViewById(R.id.repair);
    recalibrate=(Button)findViewById(R.id.recalibrate);
    emailconfig=(Button)findViewById(R.id.emailconfig);

    unit=(EditText) findViewById(R.id.unit);
    cal1=(EditText) findViewById(R.id.cal1);
    cal2=(EditText) findViewById(R.id.cal2);
    cal3=(EditText) findViewById(R.id.cal3);
    cal4=(EditText) findViewById(R.id.cal4);
    halm=(EditText) findViewById(R.id.halm);
    lalm=(EditText) findViewById(R.id.lalm);
    defrangedes=(EditText) findViewById(R.id.defrangedes);
    defunit=(EditText) findViewById(R.id.defunit);
    defiunit=(EditText) findViewById(R.id.defiunit);
    defcal1=(EditText) findViewById(R.id.defcal1);
    defcal2=(EditText) findViewById(R.id.defcal2);
    defcal3=(EditText) findViewById(R.id.defcal3);
    defcal4=(EditText) findViewById(R.id.defcal4);
    defhalm=(EditText) findViewById(R.id.defhalm);
    deflalm=(EditText) findViewById(R.id.deflalm);

    rawdata=(CheckBox) findViewById(R.id.checkbox_rawdata);
    blocktype=(RadioGroup) findViewById(R.id.blocktype);
    b0=(RadioButton) findViewById(R.id.b0);
    b1=(RadioButton) findViewById(R.id.b1);
    b2=(RadioButton) findViewById(R.id.b2);
    b3=(RadioButton) findViewById(R.id.b3);
    b4=(RadioButton) findViewById(R.id.b4);
    b5=(RadioButton) findViewById(R.id.b5);
    b6=(RadioButton) findViewById(R.id.b6);
    b7=(RadioButton) findViewById(R.id.b7);
    b8=(RadioButton) findViewById(R.id.b8);
    b9=(RadioButton) findViewById(R.id.b9);
    b10=(RadioButton) findViewById(R.id.b10);
    b11=(RadioButton) findViewById(R.id.b11);
    b12=(RadioButton) findViewById(R.id.b12);
    b13=(RadioButton) findViewById(R.id.b13);

    fgroup1=(LinearLayout) findViewById(R.id.fgroup1);
    fgroup2=(LinearLayout) findViewById(R.id.fgroup2);
    fgroup3=(LinearLayout) findViewById(R.id.fgroup3);
    fgroup4=(LinearLayout) findViewById(R.id.fgroup4);
    fgroup5=(LinearLayout) findViewById(R.id.fgroup5);
    medialoggergroup=(LinearLayout) findViewById(R.id.MediaLoggerGroup);
    timeformatgroup=(LinearLayout) findViewById(R.id.timeformatgroup);
    dateformatgroup=(LinearLayout) findViewById(R.id.dateformatgroup);
    tempunitgroup=(LinearLayout) findViewById(R.id.tempunitgroup);
    presunitgroup=(LinearLayout) findViewById(R.id.presunitgroup);
    tempoffsetgroup=(LinearLayout) findViewById(R.id.tempoffsetgroup);
    humioffsetgroup=(LinearLayout) findViewById(R.id.humioffsetgroup);
    tempoffset=(EditText) findViewById(R.id.tempoffset);
    humioffset=(EditText) findViewById(R.id.humioffset);

    tempunit=(Button) findViewById(R.id.tempunit);
    presunit=(Button) findViewById(R.id.presunit);

    relcal=(RelativeLayout) findViewById(R.id.relativeLayout1);

    f1=(CheckBox) findViewById(R.id.f1);
    f2=(CheckBox) findViewById(R.id.f2);
    f3=(CheckBox) findViewById(R.id.f3);
    f4=(CheckBox) findViewById(R.id.f4);
    f5=(CheckBox) findViewById(R.id.f5);
    f6=(CheckBox) findViewById(R.id.f6);
    f7=(CheckBox) findViewById(R.id.f7);
    f8=(CheckBox) findViewById(R.id.f8);
    f9=(CheckBox) findViewById(R.id.f9);
    fa=(CheckBox) findViewById(R.id.fa);
    fb=(CheckBox) findViewById(R.id.fb);
    fc=(CheckBox) findViewById(R.id.fc);
    fd=(CheckBox) findViewById(R.id.fd);
    fe=(CheckBox) findViewById(R.id.fe);
    ff=(CheckBox) findViewById(R.id.ff);
    fg=(CheckBox) findViewById(R.id.fg);
    fh=(CheckBox) findViewById(R.id.fh);
    fi=(CheckBox) findViewById(R.id.fi);
    fj=(CheckBox) findViewById(R.id.fj);
    fk=(CheckBox) findViewById(R.id.fk);

    prefs =PreferenceManager.getDefaultSharedPreferences(getBaseContext());

    emailconfig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { 
    	String recipient="dr.markus.hoffmann@gmx.de";
    	String subject="Information about my data logger";
    	String message="I like to support the development of this app.\n"+
    	  		"USB Dataloggerreader app version: "+applicationVersion()+"\n"+
			"My logger is a ____ (Model, Type, Brand)\n"+
    	  		"This is all information which could be retrieved from my logger:\n"+
    	  		logger.toString()+config.toString();
    	Tools.sendEmail(v.getContext(),recipient, subject, message,null);
      }
    });
    language.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {showDialog(5);}
    });
    dateformat.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {showDialog(6);}
    });
    timeformat.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {showDialog(7);}
    });
    tempunit.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {showDialog(8);}
    });
    presunit.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {showDialog(9);}
    });
  }
  @Override
  protected void onResume(){
    super.onResume();
    logger=USBDataloggerreaderActivity.getLogger();
    config=USBDataloggerreaderActivity.getConfig();

    vid.setText(""+logger.Vid+" ("+logger.Manufacturer+")");
    pid.setText(""+logger.Pid+" ("+logger.Product+")");

    if(logger.protocol==Logger.PROTO_HID) {
			repair.setVisibility(View.VISIBLE);
			repair.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { showDialog(4);}
			});
			recalibrate.setVisibility(View.GONE);
			cala.setVisibility(View.GONE);
			calb.setVisibility(View.GONE);
			unit.setVisibility(View.VISIBLE);
			cal1.setVisibility(View.GONE);
			cal2.setVisibility(View.GONE);
			cal3.setVisibility(View.GONE);
			cal4.setVisibility(View.GONE);
			halm.setVisibility(View.GONE);
			lalm.setVisibility(View.GONE);
			defrangedes.setVisibility(View.GONE);
			defiunit.setVisibility(View.GONE);
			defunit.setVisibility(View.GONE);
			defcal1.setVisibility(View.GONE);
			defcal2.setVisibility(View.GONE);
			defcal3.setVisibility(View.GONE);
			defcal4.setVisibility(View.GONE);
			defhalm.setVisibility(View.GONE);
			deflalm.setVisibility(View.GONE);
			rawdata.setVisibility(View.VISIBLE);
			blocktype.setVisibility(View.GONE);
			actual.setVisibility(View.VISIBLE);
			serial.setVisibility(View.VISIBLE);
			fversion.setVisibility(View.VISIBLE);
			battery.setVisibility(View.GONE);
			textbattery.setVisibility(View.GONE);
			path.setVisibility(View.GONE);
			textpath.setVisibility(View.GONE);
			sensor.setVisibility(View.GONE);
			textsensor.setVisibility(View.GONE);
			fgroup1.setVisibility(View.GONE);
			fgroup2.setVisibility(View.GONE);
			fgroup3.setVisibility(View.GONE);
			fgroup4.setVisibility(View.GONE);
			fgroup5.setVisibility(View.GONE);
			relcal.setVisibility(View.GONE);
			actual.setText(String.format(Locale.US,"%08x",logger.config.rawinputreading));
			fversion.setText(logger.config.getversion());
			serial.setText(String.format("%08x",logger.config.serial_number));
    } else if(logger.protocol==Logger.PROTO_ELV) {
			cala.setVisibility(View.VISIBLE);
			repair.setVisibility(View.VISIBLE);
			repair.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { showDialog(0);}
			});
			recalibrate.setVisibility(View.VISIBLE);
			recalibrate.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { showDialog(2);}
			});
			unit.setVisibility(View.VISIBLE);
			cala.setText(""+logger.config.calibration_Mvalue);
			calb.setVisibility(View.VISIBLE);
			calb.setText(""+logger.config.calibration_Cvalue);

			if(logger.config.block_type>=4) {  // EL-USB-3,4
				unit.setVisibility(View.VISIBLE);
				cal1.setVisibility(View.VISIBLE);
				cal2.setVisibility(View.VISIBLE);
				cal3.setVisibility(View.VISIBLE);
				cal4.setVisibility(View.VISIBLE);
				halm.setVisibility(View.VISIBLE);
				lalm.setVisibility(View.VISIBLE);

				defrangedes.setVisibility(View.VISIBLE);
				defunit.setVisibility(View.VISIBLE);
				defiunit.setVisibility(View.VISIBLE);
				defcal1.setVisibility(View.VISIBLE);
				defcal2.setVisibility(View.VISIBLE);
				defcal3.setVisibility(View.VISIBLE);
				defcal4.setVisibility(View.VISIBLE);
				defhalm.setVisibility(View.VISIBLE);
				deflalm.setVisibility(View.VISIBLE);

				unit.setText(config.getunit());
				cal1.setText(config.getcal1());
				cal2.setText(config.getcal2());
				cal3.setText(config.getcal3());
				cal4.setText(config.getcal4());
				halm.setText(config.gethalmtxt());
				lalm.setText(config.getlalmtxt());

				defcal1.setText(config.getdefcal1());
				defcal2.setText(config.getdefcal2());
				defcal3.setText(config.getdefcal3());
				defcal4.setText(config.getdefcal4());
				defhalm.setText(config.getdefhalmtxt());
				deflalm.setText(config.getdeflalmtxt());
				defunit.setText(config.getdefunit());
				defiunit.setText(config.getdefiunit());
				defrangedes.setText(config.getdefrange());
				fgroup5.setVisibility(View.VISIBLE);
      } else {
				unit.setVisibility(View.GONE);
				cal1.setVisibility(View.GONE);
				cal2.setVisibility(View.GONE);
				cal3.setVisibility(View.GONE);
				cal4.setVisibility(View.GONE);
				halm.setVisibility(View.GONE);
				lalm.setVisibility(View.GONE);
				defrangedes.setVisibility(View.GONE);
				defiunit.setVisibility(View.GONE);
				defunit.setVisibility(View.GONE);
				defcal1.setVisibility(View.GONE);
				defcal2.setVisibility(View.GONE);
				defcal3.setVisibility(View.GONE);
				defcal4.setVisibility(View.GONE);
				defhalm.setVisibility(View.GONE);
				deflalm.setVisibility(View.GONE);
				fgroup5.setVisibility(View.GONE);
			}
			rawdata.setVisibility(View.VISIBLE);
			blocktype.setVisibility(View.VISIBLE);
			relcal.setVisibility(View.VISIBLE);
			if(logger.loggertype==Logger.LTYP_TH || 
					logger.loggertype==Logger.LTYP_THP) {
				fgroup2.setVisibility(View.VISIBLE);
			} else {
				fgroup2.setVisibility(View.GONE);
			}
			
			b0.setChecked(config.block_type==0);
			b1.setChecked(config.block_type==1);
			b2.setChecked(config.block_type==2);
			b3.setChecked(config.block_type==3);
			b4.setChecked(config.block_type==4);
			b5.setChecked(config.block_type==5);
			b6.setChecked(config.block_type==6);
			b7.setChecked(config.block_type==7);
			b8.setChecked(config.block_type==8);
			b9.setChecked(config.block_type==9);
			b10.setChecked(config.block_type==10);
			b11.setChecked(config.block_type==11);
			b12.setChecked(config.block_type==12);
			b13.setChecked(config.block_type==13);

			f1.setChecked((config.flag_bits&Lascar.HIGH_ALARM_STATE)==Lascar.HIGH_ALARM_STATE);
			f2.setChecked((config.flag_bits&Lascar.LOW_ALARM_STATE)==Lascar.LOW_ALARM_STATE);
			f3.setChecked((config.flag_bits&Lascar.HIGH_ALARM_LATCH)==Lascar.HIGH_ALARM_LATCH);
			f4.setChecked((config.flag_bits&Lascar.LOW_ALARM_LATCH)==Lascar.LOW_ALARM_LATCH);
			f5.setChecked((config.flag_bits&Lascar.CH2_HIGH_ALARM_STATE)==Lascar.CH2_HIGH_ALARM_STATE);
			f6.setChecked((config.flag_bits&Lascar.CH2_LOW_ALARM_STATE)==Lascar.CH2_LOW_ALARM_STATE);
			f7.setChecked((config.flag_bits&Lascar.CH2_HIGH_ALARM_LATCH)==Lascar.CH2_HIGH_ALARM_LATCH);
			f8.setChecked((config.flag_bits&Lascar.CH2_LOW_ALARM_LATCH)==Lascar.CH2_LOW_ALARM_LATCH);
			f9.setChecked((config.flag_bits&Lascar.LOGGING_STATE)==Lascar.LOGGING_STATE);
			fa.setChecked((config.flag_bits&Lascar.UNREAD)==Lascar.UNREAD);
			fb.setChecked((config.flag_bits&Lascar.BATTERY_LOW)==Lascar.BATTERY_LOW);
			fc.setChecked((config.flag_bits&Lascar.BATTERY_FAIL)==Lascar.BATTERY_FAIL);
			fd.setChecked((config.flag_bits&Lascar.SENSOR_FAIL)==Lascar.SENSOR_FAIL);
			fe.setChecked((config.flag_bits&0x2000)==0x2000);
			ff.setChecked((config.flag_bits&0x4000)==0x4000);
			fg.setChecked((config.flag_bits&0x8000)==0x8000);
			fh.setChecked((config.flag_bits2&0x1)>0);
			fi.setChecked((config.flag_bits2&0x2)>0);
			fj.setChecked((config.flag_bits2&0x4)>0);
			fk.setChecked((config.flag_bits2&0x8)>0);

			serial.setVisibility(View.VISIBLE);
			serial.setText(""+config.serial_number);
			actual.setVisibility(View.VISIBLE);
			textactual.setVisibility(View.VISIBLE);
			battery.setVisibility(View.VISIBLE);
			textbattery.setVisibility(View.VISIBLE);
			path.setVisibility(View.GONE);
			textpath.setVisibility(View.GONE);
			sensor.setVisibility(View.VISIBLE);
			textsensor.setVisibility(View.VISIBLE);
			fversion.setVisibility(View.VISIBLE);
			fversion.setText(config.getversion());
			actual.setText(""+config.rawinputreading);
			if(logger.isreadconfig) {
				if((config.flag_bits&Lascar.BATTERY_FAIL)==Lascar.BATTERY_FAIL) {
					battery.setText("empty ("+config.battery_alarm+")");
					battery.setTextColor(Color.RED);
				} else if((config.flag_bits&Lascar.BATTERY_LOW)==Lascar.BATTERY_LOW) {
					battery.setText("low ("+config.battery_alarm+")");
					battery.setTextColor(Color.YELLOW);
				} else {
					battery.setText("good ("+config.battery_alarm+")");
					battery.setTextColor(Color.GREEN);
				}
			} else {
				battery.setText("unknown");
				battery.setTextColor(Color.CYAN);
			}
			sensor.setText("unknown");
    } else if(logger.protocol==Logger.PROTO_FILE) {
			repair.setVisibility(View.GONE);
			recalibrate.setVisibility(View.GONE);
			cala.setVisibility(View.GONE);
			calb.setVisibility(View.GONE);
			unit.setVisibility(View.GONE);
			cal1.setVisibility(View.GONE);
			cal2.setVisibility(View.GONE);
			cal3.setVisibility(View.GONE);
			cal4.setVisibility(View.GONE);
			halm.setVisibility(View.GONE);
			lalm.setVisibility(View.GONE);
			defrangedes.setVisibility(View.GONE);
			defiunit.setVisibility(View.GONE);
			defunit.setVisibility(View.GONE);
			defcal1.setVisibility(View.GONE);
			defcal2.setVisibility(View.GONE);
			defcal3.setVisibility(View.GONE);
			defcal4.setVisibility(View.GONE);
			defhalm.setVisibility(View.GONE);
			deflalm.setVisibility(View.GONE);

			rawdata.setVisibility(View.GONE);
			blocktype.setVisibility(View.GONE);
			actual.setVisibility(View.GONE);
			serial.setVisibility(View.VISIBLE);
			fversion.setVisibility(View.VISIBLE);
			battery.setVisibility(View.GONE);
			textbattery.setVisibility(View.GONE);
			sensor.setVisibility(View.GONE);
			textsensor.setVisibility(View.GONE);

			textactual.setVisibility(View.GONE);
			
		        fgroup1.setVisibility(View.GONE);
			fgroup2.setVisibility(View.GONE);
			fgroup3.setVisibility(View.GONE);
			fgroup4.setVisibility(View.GONE);
			fgroup5.setVisibility(View.GONE);
			relcal.setVisibility(View.GONE);
			path.setVisibility(View.VISIBLE);
			serial.setText(""+logger.ProfileId);
			path.setText(""+logger.Path);
    } else {
			repair.setVisibility(View.GONE);
			recalibrate.setVisibility(View.GONE);
			cala.setVisibility(View.GONE);
			calb.setVisibility(View.GONE);

			unit.setVisibility(View.GONE);
			cal1.setVisibility(View.GONE);
			cal2.setVisibility(View.GONE);
			cal3.setVisibility(View.GONE);
			cal4.setVisibility(View.GONE);
			halm.setVisibility(View.GONE);
			lalm.setVisibility(View.GONE);
			defrangedes.setVisibility(View.GONE);
			defiunit.setVisibility(View.GONE);
			defunit.setVisibility(View.GONE);
			defcal1.setVisibility(View.GONE);
			defcal2.setVisibility(View.GONE);
			defcal3.setVisibility(View.GONE);
			defcal4.setVisibility(View.GONE);
			defhalm.setVisibility(View.GONE);
			deflalm.setVisibility(View.GONE);

			rawdata.setVisibility(View.GONE);
			blocktype.setVisibility(View.GONE);
			actual.setVisibility(View.GONE);
			serial.setVisibility(View.GONE);
			fversion.setVisibility(View.GONE);
			battery.setVisibility(View.GONE);
			path.setVisibility(View.GONE);
			textpath.setVisibility(View.GONE);
			textbattery.setVisibility(View.GONE);
			sensor.setVisibility(View.GONE);
			textsensor.setVisibility(View.GONE);

			textactual.setVisibility(View.GONE);
			fgroup2.setVisibility(View.GONE);
			relcal.setVisibility(View.GONE);
			fgroup5.setVisibility(View.VISIBLE);
    }
    id.setText(logger.Serial_id);
    protocol.setText(""+logger.protocolstring());
    ltyp.setText(""+logger.typestring());
    packet.setText(""+logger.packet_size+" Bytes.");
    memory.setText(""+logger.memory_size+" Bytes.");
    name.setText(config.getname());
    fversion.setText(logger.FirmwareVersion);
    if(logger.hasset_timeformat()) {
      timeformatgroup.setVisibility(View.VISIBLE);
      timeformatselect.set_default(findin(timeformatidx,config.time_format));
      timeformat.setText(timeformatselect.get_selected_item());
    } else timeformatgroup.setVisibility(View.GONE);
    if(logger.hasset_dateformat()) {
      dateformatgroup.setVisibility(View.VISIBLE);
      dateformatselect.set_default(findin(dateformatidx,config.date_format));
      dateformat.setText(dateformatselect.get_selected_item());
    } else dateformatgroup.setVisibility(View.GONE);
    if(logger.hasset_tempunit()) {
      tempunitgroup.setVisibility(View.VISIBLE);
      tempunitselect.set_default(findin(tempunitidx,config.temp_unit));
      tempunit.setText(tempunitselect.get_selected_item());
    } else tempunitgroup.setVisibility(View.GONE);
    if(logger.hasset_presunit()) {
      presunitgroup.setVisibility(View.VISIBLE);
      presunitselect.set_default(findin(presunitidx,config.pres_unit));
      presunit.setText(presunitselect.get_selected_item());
    } else presunitgroup.setVisibility(View.GONE);
    if(logger.hasset_tempoffset()) {
      tempoffsetgroup.setVisibility(View.VISIBLE);
      tempoffset.setText(""+config.temp_offset);
    } else tempoffsetgroup.setVisibility(View.GONE);
    if(logger.hasset_humioffset()) {
      humioffsetgroup.setVisibility(View.VISIBLE);
      humioffset.setText(""+config.humi_offset);
    } else humioffsetgroup.setVisibility(View.GONE);

    if(logger.protocol==Logger.PROTO_FILE) {
      medialoggergroup.setVisibility(View.VISIBLE);
      loggerid.setText(""+config.loggerid);
      owner.setText(""+config.Owner);
      location.setText(""+config.Location);
      report.setText(""+config.Report);
      comment.setText(""+config.Comment);
      comment2.setText(""+config.Comment2);
      pdffilename.setText(config.Pdfname);
      languageselect.set_default(findin(languageidx,config.language));
      language.setText(languageselect.get_selected_item());
    } else medialoggergroup.setVisibility(View.GONE);
		
    // toffset.setText(""+.time_offset);
    //      latency.setText(""+settings.getFloat("pcorr_latenz", (float)7*60)/60);
    //      factor.setText(""+settings.getFloat("pcorr_fakt", (float)0.4));
  }
  @Override
  protected void onPause(){
    super.onPause();
    if(logger.protocol==Logger.PROTO_ELV) {
			// .rawinputreading=rawdata.isChecked()?1:0;
			try {
			  config.calibration_Mvalue=(float)Double.parseDouble(cala.getText().toString());
			  config.calibration_Cvalue=(float)Double.parseDouble(calb.getText().toString());
			} catch (NumberFormatException e) {config.calibration_Mvalue=0;config.calibration_Cvalue=0;}
			config.setcal1(cal1.getText().toString());
			config.setcal2(cal2.getText().toString());
			config.setcal3(cal3.getText().toString());
			config.setcal4(cal4.getText().toString());
			config.setunit(unit.getText().toString());

			// We need an Editor object to make preference changes.
			// All objects are from android.context.Context
			// SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			// SharedPreferences.Editor editor = settings.edit();
			if(b0.isChecked())  config.block_type=0;
			if(b1.isChecked())  config.block_type=1;
			if(b2.isChecked())  config.block_type=2;
			if(b3.isChecked())  config.block_type=3;
			if(b4.isChecked())  config.block_type=4;
			if(b5.isChecked())  config.block_type=5;
			if(b6.isChecked())  config.block_type=6;
			if(b7.isChecked())  config.block_type=7;
			if(b8.isChecked())  config.block_type=8;
			if(b9.isChecked())  config.block_type=9;
			if(b10.isChecked()) config.block_type=10;
			if(b11.isChecked()) config.block_type=11;
			if(b12.isChecked()) config.block_type=12;
			if(b13.isChecked()) config.block_type=13;


			if(f1.isChecked()) config.flag_bits|=Lascar.HIGH_ALARM_STATE; else config.flag_bits&=~Lascar.HIGH_ALARM_STATE;
			if(f2.isChecked()) config.flag_bits|=Lascar.LOW_ALARM_STATE; else config.flag_bits&=~Lascar.LOW_ALARM_STATE;
			if(f3.isChecked()) config.flag_bits|=Lascar.HIGH_ALARM_LATCH; else config.flag_bits&=~Lascar.HIGH_ALARM_LATCH;
			if(f4.isChecked()) config.flag_bits|=Lascar.LOW_ALARM_LATCH; else config.flag_bits&=~Lascar.LOW_ALARM_LATCH;
			if(f5.isChecked()) config.flag_bits|=Lascar.CH2_HIGH_ALARM_STATE; else config.flag_bits&=~Lascar.CH2_HIGH_ALARM_STATE;
			if(f6.isChecked()) config.flag_bits|=Lascar.CH2_LOW_ALARM_STATE; else config.flag_bits&=~Lascar.CH2_LOW_ALARM_STATE;
			if(f7.isChecked()) config.flag_bits|=Lascar.CH2_HIGH_ALARM_LATCH; else config.flag_bits&=~Lascar.CH2_HIGH_ALARM_LATCH;
			if(f8.isChecked()) config.flag_bits|=Lascar.CH2_LOW_ALARM_LATCH; else config.flag_bits&=~Lascar.CH2_LOW_ALARM_LATCH;

			if(fh.isChecked()) config.flag_bits2|=1; else config.flag_bits2&=~1;
			if(fi.isChecked()) config.flag_bits2|=2; else config.flag_bits2&=~2;
    }
    config.setname(name.getText().toString());
    if(logger.protocol==Logger.PROTO_FILE) {
      try {
        config.loggerid=(int)Double.parseDouble(loggerid.getText().toString());
      } catch (NumberFormatException e) {config.loggerid=4711;}
      config.Owner=owner.getText().toString();
      config.Location=location.getText().toString();
      config.Report=report.getText().toString();
      config.Comment=comment.getText().toString();
      config.Comment2=comment2.getText().toString();
      config.Pdfname=pdffilename.getText().toString();
      config.language=languageidx[languageselect.get_selected()];
    }
    if(logger.hasset_dateformat())
      config.date_format=dateformatidx[dateformatselect.get_selected()];
    if(logger.hasset_timeformat())
      config.time_format=timeformatidx[timeformatselect.get_selected()];
    if(logger.hasset_tempunit())
      config.temp_unit=tempunitidx[tempunitselect.get_selected()];
    if(logger.hasset_presunit())
      config.pres_unit=presunitidx[presunitselect.get_selected()];
    if(logger.hasset_tempoffset()) {
      try {
      config.temp_offset=(float)Double.parseDouble(tempoffset.getText().toString());
      } catch (NumberFormatException e) {config.temp_offset=0;}
    }
    if(logger.hasset_humioffset()) {
      try {
      config.humi_offset=(float)Double.parseDouble(humioffset.getText().toString());
      } catch (NumberFormatException e) {config.humi_offset=0;}
    }
    // editor.putBoolean("pcorr", pcorr1.isChecked());
    // editor.putBoolean("dewpoint", dewpoint1.isChecked());
    // editor.putFloat("pcorr_fakt",(float)Double.parseDouble(factor.getText().toString()));
    // editor.putFloat("pcorr_latenz",(float)Double.parseDouble(latency.getText().toString())*60);
    // Commit the edits!
    // editor.commit();
  }
  @Override
  public boolean onOptionsItemSelected(final MenuItem item) {
    switch (item.getItemId())  {
    case android.R.id.home:
      // This is called when the Home (Up) button is pressed
      // in the Action Bar.
      finish();
      return true;
    default: 
      return super.onOptionsItemSelected(item);
    }
  }



  @Override
  protected Dialog onCreateDialog(final int id) {
    Dialog dialog = null;
    if(id==0 || id==4) {
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle(getResources().getString(R.string.word_repair));
      if(id==4) builder.setMessage(Html.fromHtml("" +
	   "<h1>WARNING</h1>" +
	   "You need to understand what you are doing: " +
	   "The NC7004 Logger will not stop logging, unless you remove the battery." +
	   "The repair-Function will just reset the stored information about when the " +
	   "logger was last setup. Some of the recorded data will be preserved, and can still be read out." +
	   "It is just not possible anymore to determine where the recording has started." +
	   "It is not recommendet to do so. Do you really want to proceed?"));
      else builder.setMessage(Html.fromHtml(getResources().getString(R.string.message_repairwarning)));
      builder.setPositiveButton(getResources().getString(R.string.word_procceed), new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int which) {
		logger.do_repair=true;
		showDialog(1);
	} }); 
      builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int which) {
		logger.do_repair=false;
	} }); 
      dialog = builder.create();
      dialog.setCanceledOnTouchOutside(false);
    } 
    else if(id==1) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.message_repairinfo));
    else if(id==2) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.message_recalibrateinfo));
    else if(id==3) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.message_recalibrateinfo)); 
    else if(id==5) {
      dialog = languageselect.listselector(this,"Select Language:");
      dialog.setOnDismissListener(new OnDismissListener() {
        public void onDismiss(final DialogInterface dialog) {
          language.setText(languageselect.get_selected_item());
        }
      });
    }
    else if(id==6) {
      dialog =dateformatselect.listselector(this,"Select Date Format:");
      dialog.setOnDismissListener(new OnDismissListener() {
        public void onDismiss(final DialogInterface dialog) {
          dateformat.setText(dateformatselect.get_selected_item());
        }
      });
    }
    else if(id==7) {
      dialog =timeformatselect.listselector(this,"Select Time Format:");
      dialog.setOnDismissListener(new OnDismissListener() {
        public void onDismiss(final DialogInterface dialog) {
          timeformat.setText(timeformatselect.get_selected_item());
        }
      });
    }
    else if(id==8) {
      dialog =tempunitselect.listselector(this,"Select Temperature Unit:");
      dialog.setOnDismissListener(new OnDismissListener() {
        public void onDismiss(final DialogInterface dialog) {
          tempunit.setText(tempunitselect.get_selected_item());
        }
      });
    }
    else if(id==9) {
      dialog =presunitselect.listselector(this,"Select Pressure Unit:");
      dialog.setOnDismissListener(new OnDismissListener() {
        public void onDismiss(final DialogInterface dialog) {
          presunit.setText(presunitselect.get_selected_item());
        }
      });
    }

    return dialog;
  }	
  private String applicationVersion() {
    try { return getPackageManager().getPackageInfo(getPackageName(), 0).versionName; }
    catch (NameNotFoundException x)  { return "unknown"; }
  }
}
