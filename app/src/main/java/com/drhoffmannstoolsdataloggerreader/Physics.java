package com.drhoffmannstoolsdataloggerreader;

/* Physics.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 

import java.util.Calendar;
import java.util.Locale;

import static java.util.Locale.*;

public class Physics {
  public static float taupunkt(final float T, final float r) {
    float a,b;
    if(r<=0) return(0);
    if(T>=0) {a= (float) 7.5; b=(float) 237.3;} 
    else {a = (float) 7.6;b = (float) 240.7;}
    final float sdd=(float) (6.1078*Math.pow(10,((a*T)/(b+T))));	    
    final float dd=r/100*sdd;
    final float v=(float) Math.log10(dd/6.1078);
    final float td=b*v/(a-v);
    return td;
  }
  public static float water(final float T, final float r) {
    float a,b;
    if(r<=0) return(0);
    if(T>=0) {a= (float) 7.5; b=(float) 237.3;} 
    else {a = (float) 7.6;b = (float) 240.7;}
    final float sdd=(float) (6.1078*Math.pow(10,((a*T)/(b+T))));	    
    final float dd=r/100*sdd;
    float mw=(float) 18.016;
    float Rs=(float) 8314.3;
    float TK=(float) (T+273.15);
    float af=(float) (1e5*mw/Rs*dd/TK);
    return af;
  }
  public static double celsius2fahr(double cel ) { return (cel*9.0/5.0 + 32.); }
  public static double fahr2celsius(double fahr ) { return (fahr - 32.)*5.0/9.0; }

    /* gibt einen Formatierten String in Tagen, Stunden, Minuten und Sekunden*/
  public static String duration(int n,int sec) {
    return duration_dhms(n*sec);
  }
  public static String duration_dhms(int duration) {
    int d=duration;
    int s=d%60;
    d=(d-s)/60;
    int m=d%60;
    d=(d-m)/60;
    int h=d%24;
    d=(d-h)/24;
    return String.format(US,"%dd%02d:%02d:%02d",d,h,m,s);
  }
  public static String timestamp2datetime(long ss) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(ss*1000);
    String dateS=String.format(Locale.US,"%02d.%02d.%04d", 
  	  	    cal.getTime().getDate(),
  	  	    (cal.getTime().getMonth()+1),
  	  	    (cal.getTime().getYear()+1900));
    String timeS=String.format(Locale.US,"%02d:%02d:%02d",
  	  	    cal.getTime().getHours(),
  	  	    cal.getTime().getMinutes(),
  	  	    cal.getTime().getSeconds());
    return dateS+" "+timeS;
  }
  public static String timestamp2datetime_short(long ss) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(ss*1000);
    String dateS=String.format(Locale.US,"%02d.%02d.%04d", 
  	  	    cal.getTime().getDate(),
  	  	    (cal.getTime().getMonth()+1),
  	  	    (cal.getTime().getYear()+1900));
    String timeS=String.format(Locale.US,"%02d:%02d",
  	  	    cal.getTime().getHours(),
  	  	    cal.getTime().getMinutes());
    return dateS+" "+timeS;
  }

  public static double gerundet(double wert, int n) {
    double x=1;
    if(n==1) x=10;
    else if (n==2) x=100;
    else if (n==3) x=1000;
    else if (n==4) x=10000;
    else if (n==5) x=100000;
    return Math.round(wert*x)/x;
  }
}
