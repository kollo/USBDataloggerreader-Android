package com.drhoffmannstoolsdataloggerreader;

/* PlotView.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

/* An interactive Dataplot 
 *
 * (c) by Markus Hoffmann 2011-2016
 * 
 */ 

public class PlotView extends View  {
	private static final String TAG = PlotView.class.getSimpleName(); 
	private Paint paint;
	private int bx,by,bw,bh;
	private int sy,sw,sh;
	// private DataContent data;
	private double datax[];
	private byte flags[];
	private double datay[];
	private double datay2[];
	private double datay3[];
	private int compression[];
	private int hist1[],hist2[];
	private int histmax1,histmax2;
	private double miny1,miny2,miny3;
	private double maxy1,maxy2,maxy3;
	private double deltamin1, deltamin2,deltamin3;
	private double tpdata[]=null;
	ArrayList<Lueftungsevent> eventliste;
	ArrayList<Label> labels;
	
	private float textsize=16;
	private int darstellung=0;  /*  0=Steps 
	                                1=lines
	                                2=points
	                                3=dots
	                                4=lines+points
	                                5=candlestics 1Min
	                                6=TODO */

	private int anzdata=0;

	// private static final byte NORMAL=0;
	private static final byte BEGIN=1;


	private double xstep=1000,ystep=10,y2step=10,y3step=100;
	private String ey="Temperature [°C]",ex="Time",ey2="Humidity [%]",ey3="Pressure [hPa]";
	private String etp="Dew Point [°C]";
	private double xmin=0,xmax=16384,ymin=0,ymax=100,y2min=0,y2max=100,y3min=700,y3max=1200;
	private double xminbound=0,xmaxbound=32100;
	private GestureDetector mgd;
	private ScaleGestureDetector mscalegd;
	private boolean istime=false;
	private boolean dohist=false;
	private boolean dogrid=true;
	private boolean dotp=false;
	public boolean doevents=false;
	public boolean dolabels=false;


	public void setTPenable(boolean a)    {	dotp=a;    }
	public void setHistEnable(boolean a)  {	dohist=a;  }
	public void setGridEnable(boolean a)  { dogrid=a;  }
	public void setEventEnable(boolean a) {	doevents=a;}
	public void setLabelEnable(boolean a) {	dolabels=a;}
	public void setUnit(String a)         { ey=a;      }
	public void setTextsize(float a) {
		textsize=a;
		Log.d(TAG,"Textsize: "+a);
	}
	public void setDarstellung(int a) { darstellung=a;}
	public void setTimeX(boolean a)   { istime=a;  }

	public void setBegins(int begs[]) {
		if(begs!=null && flags!=null) {
			for(int i=0;i<begs.length;i++) {
				// Log.d("TAG","begins: "+begs[i]);
				flags[begs[i]]=BEGIN;
			}
		}
	}
	public void setEvents(ArrayList<Lueftungsevent> events) {
		eventliste=events;
	}
	public void setLabels(ArrayList<Label> l) {
		labels=l;
	}

	public void setData(final DataContent data) {
		if(data.y3enabled) setData(data.x,data.y,data.y2,data.y3,data.compression,data.anzdata);
		else setData(data.x,data.y,data.y2,data.compression,data.anzdata);
		setEvents(data.eventliste);
		setTPdata(data.tp);
		setBegins(data.begins);
		setLabels(data.labels);
	}


	public void setData(final double x[],final double y[], final double y2[],final double y3[],final int cc[],final int n) {
        eventliste=null;
		datax=x;
		flags=new byte[x.length];
		datay=y;
		datay2=y2;
		datay3=y3;
		if(cc==null) {
			compression=new int[datax.length];
			for(int i=0;i<compression.length;i++) compression[i]=1;
		} else compression=cc;
		anzdata=n;
		if(y!=null && anzdata>0) {
			double d=Math.abs(datay[anzdata-1]-datay[0]);
			maxy1=miny1=datay[0];
			if(d>0) deltamin1=d;
			else deltamin1=1;
			for(int i=1;i<anzdata;i++) {
				d=Math.abs(datay[i]-datay[i-1]);
				if(d>0) deltamin1=Math.min(d,deltamin1);
				miny1=Math.min(miny1, datay[i]);
				maxy1=Math.max(maxy1, datay[i]);
			}
			int maxanzbins=(int) ((maxy1-miny1)/deltamin1)+1;
			while(maxanzbins>4096) maxanzbins=(maxanzbins-1)/2+1;
			do {
				hist1=new int[maxanzbins];
				d=(maxy1-miny1)/(maxanzbins-1);
				for(int i=0;i<anzdata;i++) 
					hist1[(int) ((datay[i]-miny1)/d)]+=compression[i];
				if(maxanzbins<10) break;
				histmax1=0;
				for(int i=0;i<maxanzbins;i++) histmax1=Math.max(histmax1,hist1[i]);
				if(histmax1<4*maxanzbins) maxanzbins=(maxanzbins-1)/2+1;
				else break;
			} while(true);

		} else hist1=null;
		if(y2!=null && anzdata>0) {
			double d=Math.abs(datay2[anzdata-1]-datay2[0]);
			maxy2=miny2=datay2[0];
			if(d>0) deltamin2=d;
			else deltamin2=1;
			for(int i=1;i<anzdata;i++) {
				d=Math.abs(datay2[i]-datay2[i-1]);
				if(d>0) deltamin2=Math.min(d,deltamin2);
				if(datay2[i]>0) {
					miny2=Math.min(miny2, datay2[i]);
					maxy2=Math.max(maxy2, datay2[i]);
				}
			}
			int maxanzbins=(int) ((maxy2-miny2)/deltamin2)+1;
			while(maxanzbins>4096) maxanzbins=(maxanzbins-1)/2+1;
			do {
				hist2=new int[maxanzbins];
				d=(maxy2-miny2)/(maxanzbins-1);
				for(int i=0;i<anzdata;i++) { 
					if(datay2[i]>0) hist2[(int) ((datay2[i]-miny2)/d)]+=compression[i];
				}
				if(maxanzbins<10) break;
				histmax2=0;
				for(int i=0;i<maxanzbins;i++) histmax2=Math.max(histmax2,hist2[i]);
				if(histmax2<2*maxanzbins) maxanzbins=(maxanzbins-1)/2+1;
				else break;

			} while(true);
		} else hist2=null;
		if(y3!=null && anzdata>0) {
			double d=Math.abs(datay3[anzdata-1]-datay3[0]);
			maxy3=miny3=datay3[0];
			if(d>0) deltamin3=d;
			else deltamin3=1;
			for(int i=1;i<anzdata;i++) {
				d=Math.abs(datay3[i]-datay3[i-1]);
				if(d>0) deltamin3=Math.min(d,deltamin3);
				miny3=Math.min(miny3, datay3[i]);
				maxy3=Math.max(maxy3, datay3[i]);
			}
		} 
	}
	public void setData(double x[],double y[], double y2[],int cc[],int n) {
		setData(x,y,y2,null,cc,n);
	}
	public void setData(double x[],double y[], double y2[],int n) {
		setData(x,y,y2,null,null,n);
	}
	public void setData(double x[],double y[], double y2[],double y3[],int n) {
		setData(x,y,y2,y3,null,n);
	}
	public void setData(double x[],double y[],int n) {
		setData(x,y,null,null,null,n);
	}
	public void setXlabel(String label) {
		ex=label;
	}
	public void setYData(double y[], double y2[],int n) {
		setTrivialX(n);
		datay=y;
		datay2=y2;
		datay3=null;
		tpdata=null;
		anzdata=n;
		eventliste=null;
	}


	public void setTPdata(final double tp[]) {
		tpdata=tp;
	}
	public void setTestData() {
		setTrivialX(256);
		datay=new double[256];
		datay2=new double[256];
		datay3=null;
		tpdata=null;
		for(int i=0;i<256;i++) {
			datay[i]=50.0*Math.cos(i/50.0)+50;
			datay2[i]=50.0*Math.sin(i/50.0)+50;
		}
		anzdata=256;
		eventliste=null;
		setXRange(0,256);
		setXBounds(0,256*2);
		setAutoGridX();
	}

	public void setTrivialX(int n) {
		datax=new double[n];
		flags=new byte[n];
		compression=new int[n];
		for(int i=0;i<n;i++) {
			datax[i]=i;
			compression[i]=1;
		}
	}
	public void setTrivialX(int n,double interval) {
		datax=new double[n];
		flags=new byte[n];
		compression=new int[n];
		for(int i=0;i<n;i++) {
			datax[i]=i*interval;
			compression[i]=1;
		}
	}



	public void setRange(double x1,double x2,double y1,double y2) {
		xmin=x1;xmax=x2;ymin=y1;ymax=y2;
	}
	public void setXRange(double x1,double x2) {
		xmin=x1;xmax=x2;
	}
	public void setXBounds(double x1,double x2) {
		xminbound=x1;xmaxbound=x2;
	}
	public void setAutoRange() {
		setAutoRangeX();
		setAutoRangeY();
		setAutoRangeY2();
		setAutoRangeY3();
	}
	public void setAutoRangeX() {
		if(anzdata>0 && datax!=null && datax.length>=anzdata) {
			double tmin=9e100,tmax=-9e100;
			for(int i=0;i<anzdata;i++) {
				tmin=Math.min(tmin,datax[i]);
				tmax=Math.max(tmax,datax[i]);
			} 
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			setXRange(tmin,tmax);
			setXBounds(tmin,tmax);
		}
	}

	public void setAutoRangeY() {
		if(anzdata>0 && datay!=null && datay.length>=anzdata) {
			double tmin=99999,tmax=-99999;
			for(int i=0;i<anzdata;i++) {
				tmin=Math.min(tmin,datay[i]);
				tmax=Math.max(tmax,datay[i]);
				if(dotp && tpdata!=null) {
					tmin=Math.min(tmin,tpdata[i]);
					tmax=Math.max(tmax,tpdata[i]);
				}
			} 
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			setYRange(tmin,tmax);
		}
	}
	public void setAutoRangeY(double x1,double x2) {
		double tmin=99999,tmax=-99999;
		if(anzdata>0 && datay!=null && datay.length>=anzdata) {
			for(int i=0;i<anzdata;i++) {
				if(datax[i]>x1 && datax[i]<x2) {
					tmin=Math.min(tmin,datay[i]);
					tmax=Math.max(tmax,datay[i]);
					if(dotp && tpdata!=null) {
						tmin=Math.min(tmin,tpdata[i]);
						tmax=Math.max(tmax,tpdata[i]);
					}
				}
			}
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			setYRange(tmin,tmax);
		}
	}
	public void setAutoRangeY2() {
		double tmin=99999,tmax=-99999;
		if(anzdata>0 && datay2!=null && datay2.length>=anzdata) {
			for(int i=0;i<anzdata;i++) {
				if(datay2[i]>0) { /* Luftfeuchtewerte 0 und minus ignorieren */ 
					tmin=Math.min(tmin,datay2[i]);
					tmax=Math.max(tmax,datay2[i]);
				} 
			}
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			if(tmax>tmin) setY2Range(tmin,tmax);
		}
	}
	public void setAutoRangeY2(double x1,double x2) {
		double tmin=99999,tmax=-99999;
		if(anzdata>0 && datay2!=null && datay2.length>=anzdata) {
			for(int i=0;i<anzdata;i++) {
				if(datax[i]>x1 && datax[i]<x2 && datay2[i]>0) {
					tmin=Math.min(tmin,datay2[i]);
					tmax=Math.max(tmax,datay2[i]);
				}
			} 
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			if(tmax>tmin) setY2Range(tmin,tmax);
		}
	}
	public void setAutoRangeY3() {
		double tmin=99999,tmax=-99999;
		if(anzdata>0 && datay3!=null && datay3.length>=anzdata) {
			for(int i=0;i<anzdata;i++) {
				if(datay3[i]>0) {  /* Luftdruck 0 und minus ignorieren */ 
					tmin=Math.min(tmin,datay3[i]);
					tmax=Math.max(tmax,datay3[i]);
				} 
			}
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			if(tmax>tmin) setY3Range(tmin,tmax);
		}
	}
	public void setAutoRangeY3(double x1,double x2) {
		if(anzdata>0 && datay3!=null && datay3.length>=anzdata) {
			double tmin=99999,tmax=-99999;
			for(int i=0;i<anzdata;i++) {
				if(datax[i]>x1 && datax[i]<x2 && datay3[i]>0) {
					tmin=Math.min(tmin,datay3[i]);
					tmax=Math.max(tmax,datay3[i]);
				}
			} 
			tmin=Math.floor(tmin);
			tmax=Math.floor(tmax)+1;
			if(tmax>tmin) setY3Range(tmin,tmax);
		}
	}
	public void setAutoGrid() {
		setAutoGridX();
		setAutoGridY();
		setAutoGridY2();
		setAutoGridY3();
	}

	public void setAutoGridX() {
		double a=xmax-xmin;
		if(istime) {
			if(a<=20) setXGrid(1); 
			else if(a<=60) setXGrid(5); 
			else if(a<=2*60) setXGrid(10); 
			else if(a<=5*60) setXGrid(30);
			else if(a<=10*60) setXGrid(60);
			else if(a<=60*60) setXGrid(60*5);
			else if(a<=2*60*60) setXGrid(60*10);
			else if(a<=5*60*60) setXGrid(60*30);
			else if(a<=24*60*60) setXGrid(60*60);
			else if(a<=24*60*60*3) setXGrid(12*60*60);
			else if(a<=24*60*60*14) setXGrid(60*60*24);
			else setXGrid(60*60*24*7);
		} else {
			if(a<=15) setXGrid(1);
			else if(a<=50) setXGrid(5);
			else if(a<=100) setXGrid(10);
			else if(a<=300) setXGrid(50);
			else if(a<=1000) setXGrid(100);
			else if(a<=3000) setXGrid(500);
			else if(a<=10000) setXGrid(1000);
			else if(a<=30000) setXGrid(5000);
			else if(a<=100000) setXGrid(10000);
			else if(a<=300000) setXGrid(50000);
			else if(a<=1000000) setXGrid(100000);
			else if(a<=3000000) setXGrid(500000);
			else if(a<=10000000) setXGrid(1000000);
			else setXGrid(500000);
		}
	}
	public void setAutoGridY() {
		double a=ymax-ymin;
		if(a<=1.1) setYGrid(0.1);
		else if(a<=3) setYGrid(0.5);
		else if(a<=11) setYGrid(1);
		else if(a<=30) setYGrid(5);
		else if(a<=100) setYGrid(10);
		else if(a<=300) setYGrid(50);
		else if(a<=1000) setYGrid(100);
		else if(a<=3000) setYGrid(500);
		else if(a<=10000) setYGrid(1000);
		else setYGrid(5000);
	}
	public void setAutoGridY2() {
		double a=y2max-y2min;
		if(a<1.1) setY2Grid(0.1);
		else if(a<3) setY2Grid(0.5);
		else if(a<10) setY2Grid(1);
		else if(a<30) setY2Grid(5);
		else if(a<100) setY2Grid(10);
		else if(a<=300) setY2Grid(50);
		else if(a<=1000) setY2Grid(100);
		else if(a<=3000) setY2Grid(500);
		else if(a<=10000) setY2Grid(1000);
		else setY2Grid(5000);
	}
	public void setAutoGridY3() {
		double a=y3max-y3min;
		if(a<1.1) setY3Grid(0.1);
		else if(a<3) setY3Grid(0.5);
		else if(a<10) setY3Grid(1);
		else if(a<30) setY3Grid(5);
		else if(a<100) setY3Grid(10);
		else if(a<=300) setY3Grid(50);
		else if(a<=1000) setY3Grid(100);
		else if(a<=3000) setY3Grid(500);
		else if(a<=10000) setY3Grid(1000);
		else setY3Grid(5000);
	}



	public void setYRange(double x1,double x2) {
		ymin=x1;ymax=x2;
	}
	public void setY2Range(double x1,double x2) {
		y2min=x1;y2max=x2;
	}
	public void setY3Range(double x1,double x2) {
		y3min=x1;y3max=x2;
	}

	public void setGrid(double gx, double gy) {xstep=gx;ystep=gy;}
	public void setXGrid(double gx)  {xstep=gx;}
	public void setYGrid(double gy)  {ystep=gy;}
	public void setY2Grid(double gy) {y2step=gy;}
	public void setY3Grid(double gy) {y3step=gy;}

	public PlotView(Context context) {
		this(context, null, 0);
	}

	public PlotView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PlotView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);

		mscalegd = new ScaleGestureDetector(context,new MySimpleOnScaleGestureListener());
		setFocusable(true); //necessary for getting the touch events
		setFocusableInTouchMode(true);
		paint=new Paint();
		// Gesture detection

		mgd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
			public boolean onDoubleTap(MotionEvent e) {
				xmin=xminbound;
				xmax=xmaxbound;
				setAutoGridX();
				setAutoRangeY();
				setAutoRangeY2();
				setAutoRangeY3();
				setAutoGridY();
				setAutoGridY2();
				setAutoGridY3();
				invalidate();
				return false;
			}
			public void onLongPress(MotionEvent e){
				setAutoRangeY(xmin,xmax);
				setAutoRangeY2(xmin,xmax);
				setAutoRangeY3(xmin,xmax);
				setAutoGridY();
				setAutoGridY2();
				setAutoGridY3();
				invalidate();
			}
			public void onMove(float dx, float dy) {
				dx=(float)dx/(float)bw*(float)(xmax-xmin);
				if(xmin-dx<xminbound) dx=(float)(xmin-xminbound);
				else if(xmax-dx>xmaxbound) dx=(float)(xmax-xmaxbound);
				xmin-=dx;
				xmax-=dx;
				invalidate();
			}
			public void onResetLocation() {
				xmin=xminbound;
				invalidate();
			}
			private long startTime;
			private long endTime;
			private float totalAnimDx;
			private float totalAnimDy;
			private Interpolator animateInterpolator;


			public void onAnimateMove(float dx, float dy, long duration) {
				//  animateStart = new Matrix(translate);
				animateInterpolator = new OvershootInterpolator();
				startTime = System.currentTimeMillis();
				endTime = startTime + duration;
				totalAnimDx = dx;
				totalAnimDy = dy;
				post(new Runnable() {
					@Override
					public void run() {
						onAnimateStep();
					}
				});
			}
			private void onAnimateStep() {
				long curTime = System.currentTimeMillis();
				float percentTime = (float) (curTime - startTime)
						/ (float) (endTime - startTime);
				float percentDistance = animateInterpolator
						.getInterpolation(percentTime);
				float curDx = percentDistance * totalAnimDx;
				float curDy = percentDistance * totalAnimDy;
				// translate.set(animateStart);
				onMove(curDx, curDy);

				if (percentTime < 1.0f) {
					post(new Runnable() {
						@Override
						public void run() {
							onAnimateStep();
						}
					});
				}
			}
			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}
			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX, float distanceY) {
				onMove(-distanceX, -distanceY);
				return true;
			}
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				final float distanceTimeFactor = 0.1f;
				final float totalDx = (distanceTimeFactor * velocityX/2);
				final float totalDy = (distanceTimeFactor * velocityY/2);
				this.onAnimateMove(totalDx, totalDy,(long) (1000 * distanceTimeFactor));
				return false;
			}
		});
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int modex=MeasureSpec.getMode(widthMeasureSpec);
		final int sizex=MeasureSpec.getSize(widthMeasureSpec);
		final int modey=MeasureSpec.getMode(heightMeasureSpec);
		final int sizey=MeasureSpec.getSize(heightMeasureSpec);
		if (modex == MeasureSpec.UNSPECIFIED) sw=256;
		else sw=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) sh=256;
		else sh=sizey;
		setMeasuredDimension(sw, sh);
	}
	private int kx(double dux)  {return bx+(int)((dux-xmin)/(xmax-xmin)*bw);}
	private int ky(double duy)  {return by+bh-(int)((duy-ymin)/(ymax-ymin)*bh);}
	private int ky2(double duy) {return by+bh-(int)((duy-y2min)/(y2max-y2min)*bh);}
	private int ky3(double duy) {return by+bh-(int)((duy-y3min)/(y3max-y3min)*bh);}

	void drawevent(Canvas canvas,Lueftungsevent event,int i) {
		if(datax[event.idx2]>xmin && datax[event.idx]<xmax) {
			Paint npaint=new Paint(paint);
			float x1=kx(datax[event.idx]);
			float y1=ky(datay[event.idx]);
			float x2=kx(datax[event.idx2]);
			float y2=ky(Math.min(datay[event.idx]-1,datay[event.idx2]));
		//	Log.d(TAG,"Event #"+i+" "+datax[event.idx]+" "+(datax[event.idx2]-datax[event.idx]));

			npaint.setStrokeWidth(1);
			npaint.setColor(Color.argb(100,00, 255, 255));
			npaint.setStyle(Paint.Style.FILL_AND_STROKE);
			
			canvas.drawRect(x1,y1,x2,y2,npaint);

			npaint.setStyle(Paint.Style.STROKE);
			npaint.setColor(Color.argb(180,255, 0, 255));

			canvas.drawRect(x1,y1,x2,y2,npaint);
			canvas.drawText(""+i,x1,y1-textsize/4,npaint);
		}
	}
	void drawlabel(Canvas canvas,Label label) {
		Paint ppaint=new Paint(paint);
		ppaint.setAntiAlias(true);
		ppaint.setColor(Color.WHITE);
		Paint npaint=new Paint(ppaint);
		ppaint.setStrokeCap(Cap.ROUND);
		ppaint.setStrokeWidth(8);
		npaint.setStrokeWidth(1);
		npaint.setTextSize(textsize);
		float lymin,lymax;

		if(datax[label.idx]>=xmin && datax[label.idx]<xmax) {
			float x=kx(datax[label.idx]);
			lymax=lymin=ky(datay[label.idx]);
			canvas.drawPoint(x,ky(datay[label.idx]),ppaint);
			if(datay2!=null && datay2[label.idx]>0) {
				canvas.drawLine(x, ky(datay[label.idx]),x,ky2(datay2[label.idx]), npaint);
				canvas.drawPoint(x,ky2(datay2[label.idx]),ppaint);
				lymin=Math.min(ky2(datay2[label.idx]), lymin);
				lymax=Math.max(ky2(datay2[label.idx]), lymax);
				
			}
			if(datay3!=null && datay3[label.idx]>0) {
				canvas.drawLine(x, ky(datay[label.idx]),x,ky3(datay3[label.idx]), npaint);
				canvas.drawPoint(x,ky3(datay3[label.idx]),ppaint);
				lymin=Math.min(ky3(datay3[label.idx]), lymin);
				lymax=Math.max(ky3(datay3[label.idx]), lymax);
			}
			if(dotp && tpdata!=null && tpdata[label.idx]!=0) {
				canvas.drawLine(x, ky(datay[label.idx]),x,ky(tpdata[label.idx]), npaint);
				canvas.drawPoint(x,ky(tpdata[label.idx]),ppaint);	
				lymin=Math.min(ky(tpdata[label.idx]), lymin);
				lymax=Math.max(ky(tpdata[label.idx]), lymax);
			} 
			
			canvas.save();
			canvas.rotate(-90, x, lymax);
			canvas.drawText(label.name,x+(lymax-lymin-npaint.measureText(label.name))/2,lymax+textsize,npaint);
			canvas.restore();
		}
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		double x,y;
		int voi=0,i;
		String xss;
		sw=getMeasuredWidth();
		sh=getMeasuredHeight();
		bw=sw;
		bh=sh;
		by=bx=0;

		canvas.drawColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.WHITE);
		paint.setTextSize(textsize);
		// int textWidth = (int) paint.measureText("W");
		int textHeight = (int) paint.measureText("yY");
		paint.setStrokeWidth(0);
		canvas.drawRect(bx,by,bx+bw-1,by+bh-1, paint);



		int g1=(int)(xmin/xstep);
		int g2=(int)(xmax/xstep)+1;

		String dstr="";

		for(i=g1;i<g2;i++) {
			x=(double)i*xstep;
			if(dogrid) {
				paint.setColor(Color.GRAY);
				if(x>xmin && x<xmax) canvas.drawLine(kx(x),ky(ymin),kx(x),ky(ymax), paint);
			}
			paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			if(x>xmin && x<xmax) canvas.drawLine(kx(x),ky(0)+2,kx(x),ky(0)-2, paint);
			if(istime) {
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis((long)(x*1000.0));
				if(xmax-xmin<60*10) 
					xss=String.format(Locale.US,"%02d:%02d:%02d",
							cal.getTime().getHours(),
							cal.getTime().getMinutes(),
							cal.getTime().getSeconds());
				else if(xmax-xmin<=24*60*60)
					xss=String.format(Locale.US,"%02d:%02d",cal.getTime().getHours(),cal.getTime().getMinutes());
				else xss=String.format(Locale.US,"%02d",cal.getTime().getHours());

				y=(ky(0)-textHeight<sy+sh-2-textHeight)?ky(0)-textHeight:sy+sh-2-textHeight;
				y=Math.max(by+2*textHeight, y);

				if(istime) {
					String a=String.format(Locale.US,"%04d-%02d-%02d",
							(cal.getTime().getYear()+1900),
							(cal.getTime().getMonth()+1),
							cal.getTime().getDate()
							);
					if(!a.equals(dstr)) {
						if(kx(x)-paint.measureText(dstr)/2>bx) dstr=a;


						canvas.drawText(dstr,kx(x)-paint.measureText(dstr)/2,(float)y,paint);
					}
				}

			} else xss=String.format(Locale.US,"%d",(int) x);

			if(kx(x)-(int)paint.measureText(xss)/2>voi+2) {
				y=(ky(0)+textHeight<sy+sh-2)?ky(0)+textHeight:sy+sh-2;
				y=Math.max(by+textHeight, y);
				voi=(int)paint.measureText(xss);
				canvas.drawText(xss,kx(x)-voi/2,(float)y,paint);
				voi+=kx(x)-voi/2;

			}
			paint.setAntiAlias(false);
		} 
		g1=(int)(ymin/ystep);
		g2=(int)(ymax/ystep)+1;
		for(i=g1;i<g2;i++) {
			y=(double)i*ystep;
			paint.setColor(Color.argb(160,255, 50, 50));
			if(dogrid) {
				if(y>ymin && y<ymax) canvas.drawLine(kx(xmin),ky(y),kx(xmax),ky(y), paint);
			}
			//paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			if(y>ymin && y<ymax) canvas.drawLine(kx(0)+2,ky(y),kx(0)-2,ky(y), paint);

			xss=""+Math.round(y*10.0)/10.0;

			voi=(int)paint.measureText(xss);
			paint.setColor(Color.argb(255,255, 50, 50));
			canvas.drawText(xss,kx(xmin)+2,ky(y)-1,paint);

			paint.setAntiAlias(false);
		}

		if(datay2!=null) {
			g1=(int)(y2min/y2step);
			g2=(int)(y2max/y2step)+1;
			for(i=g1;i<g2;i++) {
				y=(double)i*y2step;
				paint.setColor(Color.argb(128,50, 255, 50));
				if(dogrid) {
					if(y>y2min && y<y2max) canvas.drawLine(kx(xmin),ky2(y),kx(xmax),ky2(y), paint);
				}
				paint.setAntiAlias(true);
				if(y>y2min && y<y2max) canvas.drawLine(kx(xmax)+2,ky2(y),kx(xmax)-2,ky2(y), paint);
				xss=""+y;
				voi=(int)paint.measureText(xss);
				paint.setColor(Color.argb(200,50, 255, 50));
				canvas.drawText(xss,kx(xmax)-voi-2,ky2(y)-1,paint);
				paint.setAntiAlias(false);
			}
		}
		if(datay3!=null) {
			g1=(int)(y3min/y3step);
			g2=(int)(y3max/y3step)+1;
			for(i=g1;i<g2;i++) {
				y=(double)i*y3step;
				paint.setColor(Color.argb(128,255, 255, 50));
				if(dogrid) {
					if(y>y3min && y<y3max) canvas.drawLine(kx(xmin),ky3(y),kx(xmax),ky3(y), paint);
				}
				paint.setAntiAlias(true);
				if(y>y3min && y<y3max) canvas.drawLine(kx(xmax)+2,ky3(y),kx(xmax)-2,ky3(y), paint);
				xss=""+y;
				voi=(int)paint.measureText(xss);
				paint.setColor(Color.argb(160,255, 255, 50));
				canvas.drawText(xss,kx(xmax)-voi-2,ky3(y)-1,paint);
				paint.setAntiAlias(false);
			}
		}


		// Koordinatenachsen
		paint.setColor(Color.WHITE);
		if(Math.signum(xmin)!=Math.signum(xmax)) 
			canvas.drawLine(kx(0),ky(ymin),kx(0),ky(ymax),paint);

		if(Math.signum(ymin)!=Math.signum(ymax))  
			canvas.drawLine(kx(xmin),ky(0),kx(xmax),ky(0),paint);

		for(i=-(int)textsize/2;i<=textsize/2;i++) {
			// Pfeile
			if(Math.signum(xmin)!=Math.signum(xmax)) 
				canvas.drawLine(kx(0)+i,ky(ymax)+textsize,kx(0),ky(ymax),paint);
			if(Math.signum(ymin)!=Math.signum(ymax))
				canvas.drawLine(kx(xmax)-textsize,ky(0)+i,kx(xmax),ky(0),paint);

		}
		// Beschriftung
		paint.setAntiAlias(true);

		canvas.drawText(ex,bx+bw-(int)paint.measureText(ex)-2,Math.max((ky(0)-textsize<sy+sh)?ky(0)-textsize:sy+sh-3,by+30),paint);
		paint.setColor(Color.argb(255,255, 50, 50));	
		canvas.drawText(ey,Math.max(0+2,kx(0)),by+15,paint);
		if(datay2!=null) {
			paint.setColor(Color.argb(200,50, 255, 50));
			canvas.drawText(ey2,Math.max(0,kx(xmax)-(int)paint.measureText(ey2)-8),by+15,paint);
		}
		if(datay3!=null) {
			paint.setColor(Color.argb(160,255, 255, 50));
			canvas.drawText(ey3,Math.max(0,kx(xmax)-(int)paint.measureText(ey3)-8),by+15+15,paint);
		}
		if(dotp && tpdata!=null) { 
			paint.setColor(Color.argb(200,50, 50, 255));	
			canvas.drawText(etp,Math.max(0+2,kx(0)),by+15+15,paint);
		}

		// Daten
		paint.setStrokeWidth(2);

		// Temperatur-Kurve zeichnen
		plotcurve(canvas,datay,Color.RED);

		// Events einkringeln
		if(doevents && eventliste!=null && eventliste.size()>0) {
			Log.d(TAG,"Mark "+eventliste.size()+" Events.");
			for(i=0;i<eventliste.size();i++) {
				drawevent(canvas,eventliste.get(i),i);
			}
		}
		// Ettiketten an Kurven
		if(dolabels && labels!=null && labels.size()>0) {
			Log.d(TAG,"Mark "+labels.size()+" Labels.");
			for(i=0;i<labels.size();i++) drawlabel(canvas,labels.get(i));
		}
		
		float pts[]=new float[8*anzdata];
		int count=0;

		/* Histogramm zeichnen */
		if(dohist && hist1!=null) {

			float pts2[]=new float[8*hist1.length];
			count=0;
			float a=kx(xmin),b;
			paint.setColor(Color.argb(100,255, 0, 255));
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			for(i=0;i<hist1.length;i++) {

				pts2[4*count]=a;
				pts2[4*count+1]=ky(miny1+(double)i*(maxy1-miny1)/hist1.length);
				b=(float) (kx(xmin)+0.3*((double)hist1[i]/histmax1)*(kx(xmax)-kx(xmin)));
				pts2[4*count+2]=b;
				pts2[4*count+3]=ky(miny1+(double)i*(maxy1-miny1)/hist1.length);
				count++;
				pts2[4*count]=b;
				pts2[4*count+1]=ky(miny1+(double)i*(maxy1-miny1)/hist1.length);
				pts2[4*count+2]=b;
				pts2[4*count+3]=ky(miny1+(double)(i+1)*(maxy1-miny1)/hist1.length);
				count++;
				canvas.drawRect(kx(xmin),ky(miny1+(double)i*(maxy1-miny1)/hist1.length),b,ky(miny1+(double)(i+1)*(maxy1-miny1)/hist1.length), paint);
				a=b;
			}
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(Color.argb(180,255, 0, 255));

			canvas.drawLines(pts2,0,count*4,paint);
		}


		if(datay2!=null && datay2.length>=anzdata) { 
			count=0;
			for(i=0;i<anzdata;i++) {
				if((i==anzdata-1 || datax[i+1]>xmin) && datax[i]<xmax) {
					if(i>0 && datax[i-1]<=datax[i] && flags[i]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky2(datay2[i-1]);
						pts[4*count+2]=kx(datax[i]);
						pts[4*count+3]=ky2(datay2[i]);
						count++;
					}
					if(i<anzdata-1 && datax[i+1]>datax[i] && flags[i+1]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky2(datay2[i]);
						pts[4*count+2]=kx(datax[i+1]);
						pts[4*count+3]=ky2(datay2[i]);
						count++;
					}
				} 
			}  
			paint.setColor(Color.GREEN);  
			canvas.drawLines(pts,0,count*4,paint);
		}
		/* Histogramm zeichnen */
		if(dohist && hist2!=null) {

			float pts2[]=new float[8*hist2.length];
			count=0;
			float a=kx(xmax),b;
			paint.setColor(Color.argb(100,0, 255, 255));
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			for(i=0;i<hist2.length;i++) {

				pts2[4*count]=a;
				pts2[4*count+1]=ky2(miny2+(double)i*(maxy2-miny2)/hist2.length);
				b=(float) (kx(xmax)-0.3*((double)hist2[i]/histmax2)*(kx(xmax)-kx(xmin)));
				pts2[4*count+2]=b;
				pts2[4*count+3]=ky2(miny2+(double)i*(maxy2-miny2)/hist2.length);
				count++;
				pts2[4*count]=b;
				pts2[4*count+1]=ky2(miny2+(double)i*(maxy2-miny2)/hist2.length);
				pts2[4*count+2]=b;
				pts2[4*count+3]=ky2(miny2+(double)(i+1)*(maxy2-miny2)/hist2.length);
				count++;
				canvas.drawRect(b,ky2(miny2+(double)i*(maxy2-miny2)/hist2.length),kx(xmax),
						ky2(miny2+(double)(i+1)*(maxy2-miny2)/hist2.length), paint);
				a=b;
			}
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(Color.argb(180,0,255, 255));

			canvas.drawLines(pts2,0,count*4,paint);
		}

		if(datay3!=null && datay3.length>=anzdata) { 
			count=0;
			for(i=0;i<anzdata;i++) {
				if((i==anzdata-1 || datax[i+1]>xmin) && datax[i]<xmax) {
					if(i>0 && datax[i-1]<=datax[i] && flags[i]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky3(datay3[i-1]);
						pts[4*count+2]=kx(datax[i]);
						pts[4*count+3]=ky3(datay3[i]);
						count++;
					}
					if(i<anzdata-1 && datax[i+1]>datax[i] && flags[i+1]!=BEGIN) {
						if(datax[i+1]-datax[i]>10000) {
							Log.d(TAG,"bigjump: "+i+"-->"+(i+1));
						}

						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky3(datay3[i]);
						pts[4*count+2]=kx(datax[i+1]);
						pts[4*count+3]=ky3(datay3[i]);
						count++;
					}
				} 
			}  
			paint.setColor(Color.YELLOW);
			canvas.drawLines(pts,0,count*4,paint);
		}	    
		if(dotp && tpdata!= null) plotcurve(canvas,tpdata,Color.BLUE);
		paint.setAntiAlias(false);
	}
	/* Routine zeichnet einen Datensatz */
	private void plotcurve(Canvas canvas, double datay[],int color) {
		int count=0;
		float pts[]=new float[8*anzdata];
		paint.setColor(color);  
		if(darstellung==0) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || datax[i+1]>xmin) && datax[i]<xmax) {
					if(i>0 && datax[i-1]<=datax[i] && flags[i]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky(datay[i-1]);
						pts[4*count+2]=kx(datax[i]);
						pts[4*count+3]=ky(datay[i]);
						count++;
					}
					if(i<anzdata-1 && datax[i+1]>datax[i] && flags[i+1]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky(datay[i]);
						pts[4*count+2]=kx(datax[i+1]);
						pts[4*count+3]=ky(datay[i]);
						count++;
					}
				} 
			}
			canvas.drawLines(pts,0,count*4,paint);
		} else if(darstellung==1 || darstellung==4) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || datax[i+1]>xmin) && datax[i]<xmax) {
					if(i<anzdata-1 && datax[i+1]>datax[i] && flags[i+1]!=BEGIN) {
						pts[4*count]=kx(datax[i]);
						pts[4*count+1]=ky(datay[i]);
						pts[4*count+2]=kx(datax[i+1]);
						pts[4*count+3]=ky(datay[i+1]);
						count++;
					}
				}
			}
			canvas.drawLines(pts,0,count*4,paint);
		}
		if(darstellung==2 || darstellung==3 || darstellung==4) {
			count=0;
			for(int i=0;i<anzdata;i++) {
				if(datax[i]>xmin && datax[i]<xmax) {
					pts[2*count]=kx(datax[i]);
					pts[2*count+1]=ky(datay[i]);
					count++;
				}
			}
			if(darstellung==3) canvas.drawPoints(pts, 0, 2*count, paint);
			else {
				Paint npaint=new Paint(paint);
				npaint.setStrokeCap(Cap.ROUND);
				npaint.setStrokeWidth(8);
				canvas.drawPoints(pts, 0, 2*count, npaint);
			}
		} else if(darstellung==5) {
			// TODO
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mscalegd.onTouchEvent(event);
		mgd.onTouchEvent(event);
		return true;
	}

	public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			final float scaleFactor = detector.getScaleFactor();
			double x=detector.getFocusX()/bw*(xmax-xmin)+xmin;
			xmin=(xmin-x)/scaleFactor+x;
			xmax=(xmax-x)/scaleFactor+x;
			xmax=Math.min(xmax,xmaxbound);
			xmin=Math.max(xmin,xminbound);
			//   Log.d("TAG","Scale "+detector.getScaleFactor()+" x="+x);
			setAutoGridX();
			invalidate();
			return true;
		}
	}
}
