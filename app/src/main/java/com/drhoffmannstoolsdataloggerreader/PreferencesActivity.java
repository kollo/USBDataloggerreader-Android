package com.drhoffmannstoolsdataloggerreader;

/* PreferenceActivity.java (c) 2011-2023 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 


import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.view.MenuItem;

public class PreferencesActivity  extends PreferenceActivity  implements OnSharedPreferenceChangeListener {
	private static final String TAG = "USBDL"; 
	private final static String HOMEPAGE="https://codeberg.org/kollo/USBDataloggerreader-Android";
	private final static String LICENSE_PAGE="https://codeberg.org/kollo/USBDataloggerreader-Android/raw/branch/master/LICENSE";
	private final static String SOURCE_PAGE="https://codeberg.org/kollo/USBDataloggerreader-Android";
	private final static String MANUAL_PAGE="https://codeberg.org/kollo/USBDataloggerreader-Android/src/branch/master/doc/";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		//   PreferenceManager.setDefaultValues(Preferences.this, R.xml.preferences, false);
		ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		//   actionBar.setTitle("Settings");
		findPreference("about_version").setSummary(applicationVersion());
		findPreference("about_homepage").setSummary(HOMEPAGE);
		findPreference("about_manual").setSummary(MANUAL_PAGE);

		findPreference("about_license").setSummary(LICENSE_PAGE);
		findPreference("about_source").setSummary(SOURCE_PAGE);
		findPreference("about_credits_vdl120").setSummary("http://vdl120.sourceforge.net/");
		//     findPreference("about_market_app").setSummary(String.format("market://details?id=%s", getPackageName()));
		//     findPreference("about_market_publisher").setSummary("market://search?q=pub:\"Markus Hoffmann\"");
		// Get a reference to the preferences
		//      mCheckBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_ADVANCED_CHECKBOX_PREFERENCE);
		//     mListPreference = (ListPreference)getPreferenceScreen().findPreference("select_fileformat");
		for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
			initSummary(getPreferenceScreen().getPreference(i));
		}

	}
	@Override
	protected void onResume() {
		super.onResume();
		// Set up a listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);     

	}
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
		updatePrefSummary(findPreference(key));
		if(key.equals("dewpoint")) {
			/*Eventually need a refresh in Plotting tool*/
		}
	}

	private void initSummary(Preference p){
		if (p instanceof PreferenceCategory){
			PreferenceCategory pCat = (PreferenceCategory)p;
			for(int i=0;i<pCat.getPreferenceCount();i++){
				initSummary(pCat.getPreference(i));
			}
		} else updatePrefSummary(p);
	}

	private void updatePrefSummary(Preference p){
		if (p instanceof ListPreference) {
			ListPreference listPref = (ListPreference) p; 
			p.setSummary(listPref.getEntry()); 
		}
		if (p instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) p; 
			p.setSummary(editTextPref.getText()); 
		}

	}

	private final String applicationVersion() {
		try { return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException x)  {return "unknown";}
	}

	@Override
	public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
		final String key = preference.getKey();
		if ("about_license".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LICENSE_PAGE)));
			finish();
		} 
		else if ("about_homepage".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
			finish();
		} 
		else if ("about_manual".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MANUAL_PAGE)));
			finish();
		} 
		else if ("about_version".equals(key)) {
		  startActivity(new Intent(this, InfoActivity.class));
		 //  showDialog(0);
		} else if ("about_help".equals(key))      showDialog(1);
		else if ("about_source".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
			finish();
		} else if ("about_credits_vdl120".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://vdl120.sourceforge.net/")));
			finish();
		} else if ("about_market_app".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
			finish();
		} else if ("about_market_publisher".equals(key))  {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
			finish();
		} else if ("about_hardwarevendorinfo".equals(key))  showDialog(2);
		return false;
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		if(id==1) return Tools.scrollableDialog(this,"",getResources().getString(R.string.helpdialog));
		else if(id==0) return Tools.scrollableDialog(this,"",getResources().getString(R.string.aboutdialog)+
				getResources().getString(R.string.impressum));
		else return null;
	}
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}
}
