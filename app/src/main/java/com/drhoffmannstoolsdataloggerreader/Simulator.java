package com.drhoffmannstoolsdataloggerreader;

/* Simulator.java (c) 2011-2023 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2023
 * =======================================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.util.Log;

public class Simulator {
	private static final String TAG = "SIMULATOR"; 
	private final static byte[] sim_cmdgetcfg1={0,-1,-1};
	private final static byte[] sim_cmdgetcfg2={0xf,0,0};
	private final static byte[] sim_cmdgetcfg3={0,16,1};
	private final static byte[] sim_resok64={2,0x40,0x00};
	// private final static byte[] sim_resok128={2,(byte) 0x80,0x00};
	private final static byte[] sim_resok256={2,0x00,0x01};
	private final static byte[] sim_resok180={0xf,0x1e,(byte) 0xcc};
	private final static byte[] sim_resok120={0x2,(byte) 0xd8,(byte) 0x18};
	private final static byte[] sim_resok100={0x2,(byte) 0x3e,(byte) 0x5c};

	private byte[] mem;
	private int datapointer;
	public  byte[] sim_configbuf=null;
	public  byte[] sim_data={50,0,-46,3,51,0,-46,3,54,0,-43,3,55,0,-43,3,56,0,-43,3,57,0,-43,3,59,0,-43,3,60,0,-43,3,61,0,-43,3,62,0,-43,3,64,0,-42,3,64,0,-42,3,65,0,-42,3,66,0,-42,3,67,0,-42,3,69,0,-42,3};

	private byte nextanswer[]=null;
	private byte nextnextanswer[]=null;
	
	final static byte[] config_NC7004 = {
		 -1,0x01,0x01,0x01,0x00, -1,0x01,0x05,'S','i','m','t','e','s','t','1',
		 -1,0x0e,0x01,0x11,0x15,0x19,0x22, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0x55,0x0e,0x01,0x11,0x15,0x19,0x36, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0x42,0x00,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0x42,0x00,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0x42,0x00,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,0x42,0x00,
		0x01,(byte)0xe0,0x00,0x00,0x01,0x00,0x00,0x00,0x78,0x07,0x4a,0x00,0x00,0x00,0x00,0x00,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0x02, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

	
	final static byte[] config_el_usb_1b = {2,0,66,117,114,101,97,117,77,105,107,101,45,83,0,0,0,0,16,8,41,25,1,11,
		0,0,0,0,60,0,74,0,0,0,(byte) 140,80,0,0,0,63,0,0,32,(byte) 194,0,0,0,0,118,50,46,48,121, (byte) 243, (byte) 152,
		0,0,0,0,0,0,0,0,0};
	final static byte[] config_el_usb_3b = {6 , 0 ,  69, 97,115, 121,76,111,103,32, 85, 83,  66,  0,  0,  0, // 0-15
		0 , 0 ,  18, 58, 13,   7, 9, 12,  0, 0,  0,  0,  60,  0, 71, 10, // 16-31
		0 ,  3,-116, 40,109,-102,28, 56, 41,57,-36,-71,-117,109,  0,  0, //32-47
		91, 67,  64, 52,-95,-127, 0,  0, 11, 0,  0,  0,   0,  0,  0,  0, //48-63
		86,111, 108,116,115,   0, 0,  0,  0, 0,  0,  0,  48, 46, 48, 48, //64
		0,  0,   0,  0, 48,  46,48, 48,  0, 0,  0,  0,  51, 48, 46, 48, //80
		48,  0,   0,  0, 49,  53,46, 48, 48, 0,  0,  0, 103,-93, 78, 65, //96
		53, 46,  48, 48,  0,   0, 0,  0, 48,46, 48, 48,   0,  0,  0,  0, //112
		48, 46,  48, 48, 32,  45,32, 51, 48,46, 48, 48,   0,  0, 86,111, //128
		108,116, 115,  0,  0,   0, 0,  0,  0, 0, 86,111, 108,116,115,  0,
		0,  0,   0,  0,  0,   0,48, 46, 48,48,  0,  0,   0,  0, 48, 46,
		48, 48,   0,  0,  0,   0,51, 48, 46,48, 48,  0,   0,  0, 51, 48,
		46, 48,  48,  0,  0,   0,50, 53, 46,48, 48,  0,   0,  0, 53, 46,
		48, 48,   0,  0,  0,   0,62,  2,  0, 0,  0,  0,   0,  0,  0,  0,
		0,  0,   0,  0,  0,   0, 0,  0,  0, 0,  0,  0,   0,  0,  0,  0,
		0,  0,   0,  0,  0,   0, 0,  0,  0, 0,  0,  0,   0,  0,  0,  0};

	final static byte[] config_generic={0,  0, 0,0,-128, 62,0,0,54, 6,0,0,100,0,0,0,-36,7,0,0,0,0,0,0,0,0,32,66,9, 7,17,38,56,0,-118, 87,111,104,110,122,105,109,109,101,114,0,0,0,0,0,0,2,0,0,12,66,0,0,-106,66,0,0,0,0};
	final static byte[] config_new=    {0,  0, 0,0,   0,125,0,0,31,46,0,0, 10,0,0,0,-36,7,0,0,0,0,0,0,0,0,32,66,8,25,23, 6,42,0,  10,116,117,110,110,101,108,  0,  0,  0,  0,0,0,0,0,0,0,2,0,0,12,66,0,0,-106,66,0,0,0,0};
	final static byte[] config_dl_181= {4,  0,19,7, 9,7,-36,-1,0,0,10,3,0,2,1,27,88,-16,96,3,-24,0,0,3,100,-13,-60,90,0,0,5,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//     {48,41,15,6,12,7,-35,-1,0,0,10,3,0,1,0,27,88,-16,96,3,-24,0,0,3,100,-13,-60,90,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//     {25,44,15,6,12,7,-35,-1,0,0,10,3,0,1,0,27,88,-16,96,3,-24,0,0,3,100,-13,-60,90,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}


	/* cbuf={5,10,18,12,9,7,-36,   --- Datum & Zeit
	 *      -1,0,0,
	 *      10,3,  -- LED conf & Blink Interval
	 *      0,2,   -- short interval in minuten
	 *      1,     -- start automatically
	 *      27,88,-16,96, -- Temp Schwellen
	 *      3,-24,0,0,    -- rh schwellen
	 *      3,100,-13,-60, -- pres schwellen
	 *      90,
	 *      0,0,0,-29,     -- num data conf/ num data rec
	 *      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
     cbuf={52,24,9,15,9,7,-36,
           -1,0,0,
           10,3,
           0,2,
           1,
           27,88,-16,96,
           3,-24,0,0,
           3,100,-13,-60,
           90,
           0,0,0,1,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	 */
	private final Logger logger;


	public Simulator(Logger l) {
		logger=l;
	}
	public void init(int simulate_type) {

		if(simulate_type>0 && simulate_type<=13) {
			logger.protocol=Logger.PROTO_ELV;
			logger.set_calibration((float)1,0,(float)0.5,0);
		} else if(simulate_type==0) {
			logger.protocol=Logger.PROTO_VOLTCRAFT;
			logger.packet_size=64;
			logger.memory_size=0xfe00;
			logger.set_calibration((float)0.1,0,(float)0.1,0);
		} else if(simulate_type==14) {
			logger.protocol=Logger.PROTO_VOLTCRAFT_NEW;
			logger.packet_size=64;
			logger.memory_size=0xfe00;
			logger.set_calibration((float)0.1,0,(float)0.1,0);
		} else if(simulate_type==16) {
			logger.protocol=Logger.PROTO_VOLTCRAFT_WEATHER;
			logger.loggertype=Logger.LTYP_THP;
			logger.memory_size=0x10000;
			logger.packet_size=64;
			logger.set_calibration((float)0.1,0,(float)0.1,0,(float)0.1,(float)1013.25);
		} else if(simulate_type==17) {
			logger.protocol=Logger.PROTO_HID;
			logger.loggertype=Logger.LTYP_TH;
			logger.memory_size=0x10000;
			logger.packet_size=8;
			logger.set_calibration((float)0.1,0,(float)1,0);
			mem=new byte[logger.memory_size];
			for(int i=0;i<0x100;i++) mem[i]=config_NC7004[i];
			for(int i=0x100;i<mem.length;i++) mem[i]=(byte)0xff;
			for(int i=0x100;i<0x1c0;i+=4) {
				short t=(short)(int)(400+10*(10+50*Math.cos((double)i/100)));
				
				mem[i]=(byte)((t>>8)&0xff);
				mem[i+1]=(byte)(t&0xff);
				mem[i+2]=(byte)(int)(50+50*Math.sin((double)i/100));
				mem[i+3]=1;
			}
			datapointer=0x1c0;
		} else logger.protocol=0;

		logger.Vid=-1;
		logger.Pid=-1;

		logger.Product="Logger Simulator";
		logger.Manufacturer="DrH-Soft";
		logger.Serial_id="0_"+simulate_type;

		logger.Device=null;
		logger.isconnected=true;  
		logger.mActivity.displaystatus(logger.Manufacturer+" "+logger.Product+" (#"+logger.Serial_id+") connected.",0);
	}

	public int receive(byte[] cbuf) {
		Log.d(TAG,"got "+cbuf.length+" Bytes.");
		Log.d(TAG,"got <"+cbuf[0]+","+cbuf[1]+","+cbuf[2]+">");
		if(logger.protocol==Logger.PROTO_ELV) {
			if(cbuf.length>=3 && cbuf[0]==sim_cmdgetcfg1[0] && cbuf[1]==sim_cmdgetcfg1[1] && cbuf[2]==sim_cmdgetcfg1[2]) {
				Log.d(TAG,"was sendconfig request");
				if(logger.simulate_type==2) {
					if(sim_configbuf==null) sim_configbuf=config_el_usb_1b.clone();
					nextanswer=sim_resok64;
					nextnextanswer=sim_configbuf;
				} else if(logger.simulate_type==6) {
					if(sim_configbuf==null) sim_configbuf=config_el_usb_3b.clone();
					nextanswer=sim_resok256;
					nextnextanswer=sim_configbuf;

				}
			}
		} else if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER){
			if(cbuf.length>=3 && cbuf[0]==sim_cmdgetcfg2[0] && cbuf[1]==sim_cmdgetcfg2[1] && cbuf[2]==sim_cmdgetcfg2[2]) {
				if(sim_configbuf==null) sim_configbuf=config_dl_181.clone();
				nextanswer=sim_resok180;
				nextnextanswer=sim_configbuf;
			}
		} else if(logger.protocol==Logger.PROTO_VOLTCRAFT_NEW) {
			if(cbuf.length>=3 && cbuf[0]==sim_cmdgetcfg3[0] && cbuf[1]==sim_cmdgetcfg3[1] && cbuf[2]==sim_cmdgetcfg3[2]) {
				if(sim_configbuf==null) sim_configbuf=config_new.clone();
				nextanswer=sim_resok100;
				nextnextanswer=sim_configbuf;
			}
		} else {
			if(cbuf.length>=3 && cbuf[0]==sim_cmdgetcfg3[0] && cbuf[1]==sim_cmdgetcfg3[1] && cbuf[2]==sim_cmdgetcfg3[2]) {
				if(sim_configbuf==null) sim_configbuf=config_generic.clone();
				nextanswer=sim_resok120;
				nextnextanswer=sim_configbuf;
			} else if(cbuf.length>=3 && cbuf[0]==0 && cbuf[1]==0 && cbuf[2]==64) {
				nextanswer=sim_resok120;
				nextnextanswer=sim_data;
			} else Log.d(TAG,"Unknown message.");
		}
		return (cbuf.length);
	}
	byte[] HIDoutstack[];
	int HIDoutsp=0;
	public void sendHIDCommand(int control,int bank, int adr,int data) {
		byte[] ret=new byte[8];
		if(control==0xa3) {
			mem[bank<<8+adr]=(byte)data;
			for(int i=0;i<8;i++) ret[i]=(byte)0xa5;
			// HIDoutstack[HIDoutsp++]=ret;
			// TODO:
		}
	}
	public void writeByte(int adr, byte a) {
		mem[adr&0xffff]=a;
	}
	public void writeBytes(int adr, byte[] buf) {
		for(int i=0;i<buf.length;i++) mem[(adr+i)&0xffff]=buf[i];
	}
	public byte[] readBytes(int adr, int n) {
		byte[] buf=new byte[n];
		for(int i=0;i<n;i++) buf[i]=mem[(adr+i)&0xffff];
		return(buf);
	}
	public byte readByte(int adr) {
		return(mem[adr&0xffff]);
	}
	public final static byte[] HIDOK={(byte)0xa5,(byte)0xa5,(byte)0xa5,(byte)0xa5,
		(byte)0xa5,(byte)0xa5,(byte)0xa5,(byte)0xa5};
	public void sendHIDData(byte[] message) {
		// TODO:
	}
	public byte [] receiveHID() {
		  byte[] cbuf=new byte[8];
		  // TODO:
		  return(cbuf);
	}
	public int send(byte[] cbuf) {
		if(nextanswer==null) return Error.ERR;
		else {
			int ret=Math.min(nextanswer.length,cbuf.length);
			int i;
			for(i=0;i<ret;i++) {
				cbuf[i]=nextanswer[i];
			}
			nextanswer=nextnextanswer;
			nextnextanswer=null;
			return ret;
		}
	}
	public int send(byte[] cbuf,int n) {
		if(nextanswer==null) return Error.ERR;
		else {
			int ret=Math.min(nextanswer.length,Math.min(cbuf.length,n));
			int i;
			for(i=0;i<ret;i++) {
				cbuf[i]=nextanswer[i];
			}
			nextanswer=nextnextanswer;
			nextnextanswer=null;
			return ret;
		}
	}
}
