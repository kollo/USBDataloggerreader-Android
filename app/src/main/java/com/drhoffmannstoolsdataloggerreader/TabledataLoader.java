package com.drhoffmannstoolsdataloggerreader;

/* TabledataLoader.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann
 * ==============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.os.AsyncTask;
import android.util.Log;

public class TabledataLoader  extends AsyncTask<Integer, Integer, Integer> {
  private static final String TAG = "Tabledataloader";
  private int mFilesComplete,mAnzfiles;
  private boolean mCompleted=false;
  private boolean mCancelled=false;
  private boolean isalive=false;
  private TabletoolActivity mActivity;
  DataContent data;
  String[] mFileList;
  boolean[] mcheckitems; 


  /**
   * Return a new AsyncTask that is connected to the activity given. 
   *
   * @param context MainActivity - the activity that this task is working with
   */

  public TabledataLoader (TabletoolActivity context, String[] FileList,boolean[] checkitems) {
    mActivity = context;
    mFilesComplete=0;
    mFileList=FileList;
    mcheckitems=checkitems;
    mAnzfiles=0;

    for(int i=0;i<checkitems.length;i++) {
     	if(checkitems[i]) mAnzfiles++;
    }
    data=new DataContent(20736);  // 16384
  } 
  /**
   * Disconnect the task from the activity it was set up to work with.
   * Doing this means that no more calls are made to update the UI thread.
   * This is done automatically in onCancelled and onPostExecutre when the task ends. 
   * In the UI thread, you can use this method from your activity code.
   * 
   */

  public void disconnect () {
    if (mActivity != null) {
      mActivity = null;
      Log.d (TAG, "Tableloader has successfully disconnected from the activity.");
    }
  } 

  public boolean isAlive() { return isalive; }

  @Override
  protected Integer doInBackground(Integer... params) {
    int i=0;
    isalive=true;
    Log.d(TAG, "inBackground ...");
    data.clear();
    if(mFileList.length>0) {
      for(i=mFileList.length-1;i>=0;i--) {
    	if(mcheckitems[i]) {
    	  // Show progress on the UI thread.  
    	  publishProgress(i*1000+100*mFilesComplete/mAnzfiles);
    	  data.loaddatafile(mFileList[i]);
    	  data.shrinkdata();
    	  mFilesComplete++;
    	}
      }

      publishProgress(-1);
      data.analyze();
    
      if(data.anzdata>0) {
    	// data.tpdewpoint();
    	data.tpwater();
      } 
      data.calc_events(); 		    
    }

    Log.d(TAG, "inBackground completed.");
    isalive=false;
    return i;
  }

  @Override 
  protected void onProgressUpdate(Integer... info) {
    if (mActivity != null) mActivity.updateProgress(info [0]);
  }
	
  /**
   * Report that the task has been cancelled.
   * This method runs in the UI thread.
   */

  @Override 
  protected void onCancelled () {
    mCompleted = true;
    mCancelled = true;
    isalive=false;
    if (mActivity != null) mActivity.loadDataComplete (data, mCancelled);
    disconnect();
  }
	
  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    if (mActivity != null) mActivity.prepare_progressdialog();
  }
  /**
   * Report that the task has finished.
   * This method runs in the UI thread.
   */

  @Override 
  protected void onPostExecute (Integer result)  {
    mCompleted = true;
    if (mActivity != null) mActivity.loadDataComplete (data, mCancelled);
    disconnect();
  }


  public void resetActivity (TabletoolActivity activity) {
    mActivity = activity;

    // If the task has completed during the configuration change, assume the
    // activity does not know that and tell it.
    if (mCompleted && (mActivity != null)) {
      mActivity.loadDataComplete(data, mCancelled);
      disconnect();
    }
  }	  
}
