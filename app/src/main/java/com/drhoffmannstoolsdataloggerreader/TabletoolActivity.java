package com.drhoffmannstoolsdataloggerreader;

/* TableToolActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann
 * ==============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.util.Calendar;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.util.TypedValue;

public class TabletoolActivity  extends Activity {
    private static final String TAG = TabletoolActivity.class.getSimpleName();
    private TabledataLoader mLoaderTask = null;
    private ProgressDialog pd;
    private static DataContent mdata=null;
    private static DataCondensat mdatac=null;
    private static FileSelect fileselect=new FileSelect();
    TableLayout mtable=null;
    TableLayout legende=null;
    TextView message1;
    TextView message2;
    ScaleGestureDetector mscalegd;
    int displaylevel=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabletool);
		pd = new ProgressDialog(this);
		pd.setIndeterminate(true);
		pd.setCancelable(false);
		pd.setTitle(getResources().getString(R.string.word_loaddata));
		pd.setMessage(getResources().getString(R.string.word_readfiles));
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(getResources().getString(R.string.word_tabletooltitle));
		}
		mtable= (TableLayout) findViewById(R.id.maintable);
		legende= (TableLayout) findViewById(R.id.legende);
		message1= (TextView) findViewById(R.id.message1);
		message2= (TextView) findViewById(R.id.message2);

		mscalegd = new ScaleGestureDetector(this,new MySimpleOnScaleGestureListener());
		mtable.setFocusable(true); //necessary for getting the touch events
		mtable.setFocusableInTouchMode(true);
		mtable.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				if(!mscalegd.isInProgress()) mscalegd.onTouchEvent(event);
				return true;
			}
		});


		// There are two situations where onCreate is called: (a) a completely new activity;
		// (b) a new activity created as part of a change in orientation.
		// We can tell the difference by looking at the result of getLastNonConfigurationInstance.
		Object retained = getLastNonConfigurationInstance ();
		if (retained == null) {
			// If there is no retained object, no DataLoading is in Progress
			if(mdata==null) {
				fileselect.loadFileList();
				refresh();
			}
		} else {       
			// If there is a task already running, connect it to the this new activity object.
			mLoaderTask= (TabledataLoader) retained;
			mLoaderTask.resetActivity (this);
			if(mLoaderTask.isAlive()) {
				pd.show();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		//  plot.setHistEnable(prefs.getBoolean("histograms", false));
		//  plot.setGridEnable(prefs.getBoolean("grid", true));
		if(mLoaderTask==null || !mLoaderTask.isAlive()) {
			if(mdata!=null) updateTable(mdata);
		}
	}

	@Override
	public void onDestroy () {
		super.onDestroy ();
		if (isFinishing ()) endBackgroundTasks (true);
	}

	/**
	 * onRetainNonConfigurationInstance
	 * Called by the system, as part of destroying an activity due to a configuration change, 
	 * when it is known that a new instance will immediately be created for the new configuration.
	 * Return an object that can be retrieved with getLastNonConfigurationInstance.
	 *
	 * @return Object - object to be saved
	 */

	public Object onRetainNonConfigurationInstance () {
		if (mLoaderTask != null) {
			mLoaderTask.resetActivity (null);
		}
		return mLoaderTask;
	}
	/**
	 * End any background tasks that might be running and clean up.
	 * 
	 * @param cleanup boolean - true means that any thread objects should be released
	 */

	private void endBackgroundTasks (boolean cleanup) {
		if(mLoaderTask != null) {
			// If we are cleaning up, it means that the UI is no longer available or will soon be unavailable.
			if(cleanup) {mLoaderTask.disconnect();}
			// Make sure the task is cancelled.
			mLoaderTask.cancel (true);
			// Finish cleanup by removing the reference to the task
			if (cleanup) {
				mLoaderTask = null;
				Log.d(TAG,"endBackgroundTasks: Interrupted and ended task.");
			} else Log.d(TAG,"endBackgroundTasks: Interrupted task.");
		}
	}

	/*This triggers the load Data thread in the background
	 * */

	public void refresh() {
		TabledataLoader t = mLoaderTask;
		if (t != null) {
			Log.d(TAG,"need to wait for the previous task to finish.");
		} else {
			/*Korrekturdaten lesen:*/
			//	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			//	boolean dopcorr=prefs.getBoolean("pcorr", false);
			//	float pcorrfactor=prefs.getFloat("pcorr_fakt", (float)0.4);
			//	float pcorrlatency=prefs.getFloat("pcorr_latenz", (float)7*60);
			TabledataLoader rct = new TabledataLoader (this, fileselect.mFileList,fileselect.mcheckitems);
			mLoaderTask = rct;
			// Start the task. Pass two arguments (integer).  but this is just a
			// mock-up so the values do not really matter. 
			Log.d(TAG,"Excecute Task.");
			rct.execute(4711, 815);
		}

	}

	/*This will be called by the background task before doing something*/

	public void prepare_progressdialog() {
		pd.setProgress(0);
		pd.setIndeterminate(false);
		pd.show();
	}

	public void updateProgress(int i) {
		if(i>=0) {
			pd.setProgress(i%1000);
			pd.setMessage(fileselect.mFileList[i/1000]);
		}
		else {
			pd.setProgress(100);
			pd.setMessage(getResources().getString(R.string.word_processing));
		}
	}
	private void updateTable(DataContent data) {
		Calendar cal = Calendar.getInstance();
		int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
		int width1 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
		int width2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
		TableRow.LayoutParams params0 = new TableRow.LayoutParams(width1, height);
		TableRow.LayoutParams params1 = new TableRow.LayoutParams(width2, height);
		if(mtable!=null) mtable.removeAllViews();
		if(legende!=null) legende.removeAllViews();

		/*Mache Legende für die Tabelle*/

		TableRow row = new TableRow(this);
		for (int j = 0; j < 6; j++) {
			TextView text = new TextView(this);
			text.setTextColor(Color.WHITE);

			if(j==0) text.setLayoutParams(params0);
			else text.setLayoutParams(params1);
			if(j==0) text.setText("Date");
			else if(j==1) text.setText("T [°C]");
			else if(j==2) text.setText("rh [%]");
			else if(j==3) text.setText("p [hPa]");
			else if(j==4 && data.tp!=null) text.setText("dp [°C]");
			else if(j==5) text.setText("#");
			else text.setText("");
			row.addView(text);
		}
		// row.setLongClickable(true);
		if(legende!=null) legende.addView(row);
		if(displaylevel==0) {
			boolean flags[]=new boolean[data.anzdata];
			if(data.begins!=null) {
				for(int i=0;i<data.begins.length;i++) flags[data.begins[i]]=true;
			}
			for (int i = 0; i < (data.anzdata<256?data.anzdata:256); i++) {
				row = new TableRow(this);
				for (int j = 0; j < 6; j++) {
					TextView text = new TextView(this);
					text.setTextColor(Color.YELLOW);
					if(j==0) text.setLayoutParams(params0);
					else text.setLayoutParams(params1);
					if(j==0) {
						cal.setTimeInMillis((long)data.x[i]*1000);
						String dateS=String.format(Locale.US,"%02d.%02d.%04d", 
								cal.getTime().getDate(),
								(cal.getTime().getMonth()+1),
								(cal.getTime().getYear()+1900));
						String timeS=String.format(Locale.US,"%02d:%02d:%02d",
								cal.getTime().getHours(),
								cal.getTime().getMinutes(),
								cal.getTime().getSeconds());
						text.setText(String.format(Locale.US,"%s %s", dateS,timeS));
						if(flags[i]) text.setTextColor(Color.GREEN);
						else if(data.isinevent(data.x[i])) text.setTextColor(Color.CYAN);
						else text.setTextColor(Color.YELLOW);
					}
					else if(j==1) {
						if(data.y[i]<0) text.setTextColor(Color.CYAN);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+data.y[i]);
					}
					else if(j==2) {
						if(data.y2==null || data.y2[i]<=0) text.setTextColor(Color.DKGRAY);
						if(data.y2!=null) text.setText(""+data.y2[i]);
						else text.setText("");
					}
					else if(j==3) {
						if(data.y3==null || data.y3[i]<=0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						if(data.y3!=null) text.setText(""+data.y3[i]);
						else text.setText("");
					}
					else if(j==4 && data.tp!=null) {
						double tp;
						if(data.y2==null || data.y2[i]<=0) {
							text.setTextColor(Color.DKGRAY);
							tp=0;
						} else {
							tp=Physics.taupunkt((float)data.y[i],(float)data.y2[i]);
							if(data.y[i]-tp<3) text.setTextColor(Color.CYAN);
						}
						text.setText(""+Physics.gerundet(tp,2));
					}
					else if(j==5) text.setText(""+data.compression[i]);
					else text.setText(String.format(Locale.US,"(%d, %d)", i, j));
					row.addView(text);
				}
				if(mtable!=null) mtable.addView(row);
			}
			message1.setText(""+data.anzdata+" lines. dl="+displaylevel);
			message2.setText(""+data.anzsamples+" samples in "+data.nfiles+" Files. T=["+
					data.ymin+":"+data.ymax+"], rh=["+data.y2min+":"+data.y2max+"], p=["+
					data.y3min+":"+data.y3max+"], "+(data.eventliste!=null?data.eventliste.size():0)+" Events.");
		} else if(mdatac!=null){ /*Displaylevel > 0*/
			for (int i = 0; i < (mdatac.y.length<256?mdatac.y.length:256); i++) {
				row = new TableRow(this);
				for (int j = 0; j < 6; j++) {
					TextView text = new TextView(this);
					text.setTextColor(Color.argb(255,255, 128, 0));
					if(j==0) text.setLayoutParams(params0);
					else text.setLayoutParams(params1);
					if(j==0) {
						cal.setTimeInMillis((long)(mdatac.startx+mdatac.bin*i)*1000);
						String dateS=String.format(Locale.US,"%02d.%02d.%04d", 
								cal.getTime().getDate(),
								(cal.getTime().getMonth()+1),
								(cal.getTime().getYear()+1900));
						if(displaylevel==1) {
							String timeS=String.format(Locale.US,"%02d:%02d",
									cal.getTime().getHours(),
									cal.getTime().getMinutes());
							text.setText(String.format(Locale.US,"%s %s", dateS,timeS));
						} else if(displaylevel==2) {
							String timeS=String.format(Locale.US,"%02dh",
									cal.getTime().getHours());
							text.setText(String.format(Locale.US,"%s %s", dateS,timeS));
						} else text.setText(String.format("%s", dateS));
					}
					else if(j==1) {
						if(mdatac.y[i]<0) text.setTextColor(Color.CYAN);
						else text.setTextColor(Color.YELLOW);
						if(mdatac.ymin[i]==9999) text.setTextColor(Color.DKGRAY);
						text.setText(""+mdatac.ymin[i]+":"+mdatac.ymax[i]);
					}
					else if(j==2) {
						if(mdatac.y2==null || mdatac.y2[i]<=0) text.setTextColor(Color.DKGRAY);
						text.setText(""+mdatac.y2min[i]+":"+mdatac.y2max[i]);
					}
					else if(j==3) {
						if(mdatac.y3==null || mdatac.y3[i]<=0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+mdatac.y3min[i]+":"+mdatac.y3max[i]);
					}
					else if(j==4 && data.tp!=null) {
						double tp;
						if(mdatac.y2==null || mdatac.y2[i]<=0) {
							text.setTextColor(Color.DKGRAY);
							tp=0;
						} else {
							tp=Physics.taupunkt((float)mdatac.y[i],(float)mdatac.y2[i]);
							if(mdatac.y[i]-tp<3) text.setTextColor(Color.CYAN);
						}
						text.setText(""+Physics.gerundet(tp,2));
					}
					else if(j==5) text.setText(""+mdatac.events[i]+"/"+mdatac.samples[i]);
					else text.setText(String.format(Locale.US,"(%d, %d)", i, j));
					row.addView(text);
				}
				if(mtable!=null) mtable.addView(row);
			}
			if(displaylevel==1) message1.setText("minute summary.");
			else if(displaylevel==2) message1.setText("hourly summary.");
			else if(displaylevel==3) message1.setText("daily summary.");
			else if(displaylevel==4) message1.setText("weekly summary.");
			else message1.setText("monthly summary.");

			message2.setText(""+mdatac.anzdata+" samples. T=["+
					data.ymin+":"+data.ymax+"], rh=["+data.y2min+":"+data.y2max+"], p=["+
					data.y3min+":"+data.y3max+"], "+(data.eventliste!=null?data.eventliste.size():0)+" Events.");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.tabletoolmenu, menu);
		return true;
	}
	private int dialogi=0;
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		case R.id.vdl_options_preferences:
			startActivity(new Intent(this, PreferencesActivity.class));
			return true;
		case R.id.vdl_options_about:
			showDialog(0);
			return true;
		case R.id.vdl_options_help:
			showDialog(1);
			return true;
		case R.id.vdl_options_finish:
			finish();
			return true; 
		case R.id.vdl_options_normalview:
			displaylevel=0;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_minuteview:
			displaylevel=1;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_hourview:
			displaylevel=2;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_dayview:
			displaylevel=3;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_weekview:
			displaylevel=4;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_monthview:
			displaylevel=5;
			recalccondensat();
			updateTable(mdata);
			mtable.invalidate();
			return true;
		case R.id.vdl_options_refresh:
			refresh();
			return true;
		case R.id.vdl_options_choosefile:
			fileselect.loadFileList();
			mdata=null;
			showDialog(10+dialogi++);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==1) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.tabletoolhelpdialog));
		else if(id==0) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.aboutdialog)+
			getResources().getString(R.string.impressum));
		else dialog=fileselect.fileselector(this);
		return dialog;
	}
	private void adaptdisplaylevel() {
		/*Stelle sicher, dass genügend Punkte für die Aggregation da sind*/
		if(displaylevel==1 && mdata.mindx>30) displaylevel=2;
		if(displaylevel==2 && mdata.mindx>30*60) displaylevel=3;
		if(displaylevel==3 && mdata.mindx>12*60*60) displaylevel=4;
		if(displaylevel==4 && mdata.mindx>3.5*60*60*24) displaylevel=5;
	}
	private void recalccondensat() {
		if(displaylevel==0) mdatac=null;
		else if(displaylevel==1) mdatac=mdata.getCondensate(60);
		else if(displaylevel==2) mdatac=mdata.getCondensate(60*60);
		else if(displaylevel==3) mdatac=mdata.getCondensate(60*60*24);
		else if(displaylevel==4) mdatac=mdata.getCondensate(60*60*24*7);
		else mdatac=mdata.getCondensate(60*60*24*30);
	}
	public void loadDataComplete(DataContent data,boolean cancel) {
		if(cancel) Log.d(TAG,"LoadData Task cancelled after " + data.nfiles + " Files. anzdata="+data.anzdata);
		else Log.d(TAG,"LoadData Task ended after " + data.nfiles + " Files. anzdata="+data.anzdata);
		mLoaderTask.disconnect ();
		mLoaderTask = null;
		if(!cancel && data.anzdata>0) {
			mdata=data;
			adaptdisplaylevel();
			recalccondensat();
		}
		updateTable(data);
		pd.dismiss();
	}

	public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			final float scaleFactor = detector.getScaleFactor();
			double x=detector.getFocusX();
			int olddl=displaylevel;
			Log.d(TAG,"Scale: "+scaleFactor+" x="+x);
			if(scaleFactor>1 && displaylevel>0) displaylevel--;
			else if(scaleFactor<1 && displaylevel<5) displaylevel++;
			if(mdata!=null) {
				adaptdisplaylevel();
				if(displaylevel!=olddl) {
					recalccondensat();
					updateTable(mdata);
				    mtable.invalidate();
				}
	    }
	    return true;
	}
    }
}
