package com.drhoffmannstoolsdataloggerreader;

/* Tools.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 
 * ==============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.File;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.ActivityNotFoundException;

public class Tools {
	private static final String TAG = Tools.class.getSimpleName(); 

	public static void sendEmail(Context context,String recipient, String subject, String message, String filename) {
		try {
			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.setType("plain/text");
			if (recipient != null)  emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{recipient});
			if (subject != null)    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			if (message != null)    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
			if(filename!=null) { 
				File f=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+filename);
				// f.setReadable(true, false);
				if(f.exists()) emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
				else Log.d(TAG,"File not found:"+f.getAbsolutePath());
			}
			context.startActivity(Intent.createChooser(emailIntent, "Send data as mail..."));
		} catch (ActivityNotFoundException e) {
			// cannot send email for some reason
			Toast.makeText(context,"cannot send email for reason: "+e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	public static void  showMarketPage(Context c,final String packageName) {
		final Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", packageName)));
		if (c.getPackageManager().resolveActivity(marketIntent, 0) != null)
			c.startActivity(marketIntent);
		else
			c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://market.android.com/details?id=%s", packageName))));
	} 
	public static Dialog scrollableDialog(Context c,String title, String text) {
		final Dialog dialog = new Dialog(c);
		if(title.isEmpty()) dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.maindialog);
		
		TextView wV= (TextView) dialog.findViewById(R.id.TextView01);
		wV.setText(Html.fromHtml(text));
		//set up button
		Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}
	public static Dialog scrollableHelpDialog(Context c,String title, String text,final Logger logger, final USBDataloggerreaderActivity god) {
		final Dialog dialog = new Dialog(c);
		if(title.isEmpty()) dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.maindialog);
		
		TextView wV= (TextView) dialog.findViewById(R.id.TextView01);
		wV.setText(Html.fromHtml(text));
		//set up button
		Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			  dialog.dismiss();
			  USBDataloggerreaderActivity.helpdialogisopen=false;
			  if(Build.VERSION.SDK_INT >Build.VERSION_CODES.LOLLIPOP) { /* 21 */
			    /* Wenn noch kein Logger erkannt wurde, dann versuche, 
			      eine .CL1 Datei auszuwählen.*/
			    if(!logger.isconnected && !logger.treeopened) {
			      god.openFile();
			      logger.treeopened=true;
			    }
			  }
			}
		});
		return dialog;
	}
	
  //  /* Sollte nach Erlaubnis fragen, die Datei zu schreiben etc.... */
  //  private void createFile(String mimeType, String fileName) {
  //      Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
  //      intent.addCategory(Intent.CATEGORY_OPENABLE);
  //      intent.setType(mimeType);
  //      intent.putExtra(Intent.EXTRA_TITLE, fileName);
  //      startActivityForResult(intent, WRITE_REQUEST_CODE);
  //  }
	
	
   public static boolean isExternalStorageReadOnly() {  
     String extStorageState = Environment.getExternalStorageState();  
     if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {  
       return true;  
     }  
     return false;  
   }  
   public static boolean isExternalStorageAvailable() {  
     String extStorageState = Environment.getExternalStorageState();  
     if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {  
       return true;  
     }  
     return false;  
   }  	
}
