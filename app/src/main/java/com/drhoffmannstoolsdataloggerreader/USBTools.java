package com.drhoffmannstoolsdataloggerreader;

/* USBTools.java (c) 2011-2023 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2023
 * =======================================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.util.Log;

public class USBTools {
	private static final String TAG="USBTools";

	private static final String DEVICE_START = "__DEV_START__";
	private static final String DEVICE_END = "__DEV_END__";
	private static final String COMMAND = "for DEVICE in /sys/bus/usb/devices/*; do " +
			" echo "+ DEVICE_START + ";" +
			// " echo  $DEVICE;" +
			// " ls -al $DEVICE/* ;"+
			" cat $DEVICE/idVendor;"+
			" cat $DEVICE/idProduct;"+
			" cat $DEVICE/serial;"+
			" cat $DEVICE/manufacturer;"+
			" cat $DEVICE/product;"+ 
			" echo "+ DEVICE_END + ";" +
			" done";

	public static String exec(String cmd) {
		try {

			Process process = Runtime.getRuntime().exec("sh");
			DataInputStream is = new DataInputStream(process.getInputStream());
			DataOutputStream os = new DataOutputStream(process.getOutputStream());
			os.writeBytes(cmd + "\n");
			os.writeBytes("exit\n");
			os.flush();
			os.close();

			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			try {
				String fullOutput = "";
				String line;
				while ((line = reader.readLine()) != null) {
					fullOutput = fullOutput + line + "\n";
				}
				return fullOutput;
			} catch (IOException e) {
				Log.e(TAG, "exec, IOException 1");
				e.printStackTrace();
			}

			process.waitFor();

		} catch (IOException e) {
			Log.e(TAG, "exec, IOException 2");
			e.printStackTrace();

		} catch (InterruptedException e) {
			Log.e(TAG, "exec, InterruptedException");
			e.printStackTrace();
		}
		return "";
	}
	public static String getUsbInfoViaShell(){
		String res = exec(COMMAND);
		res = res.replace("\n", "\",\"");
		res = res.replace(",\""+DEVICE_END+"\",\"", "\n");
		res = res.replace(DEVICE_START+"\"\n", "");
		res = res.replace(DEVICE_START+"\",", "");
		return res;
	}

}
