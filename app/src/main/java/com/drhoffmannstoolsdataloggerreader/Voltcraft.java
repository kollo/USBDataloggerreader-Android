package com.drhoffmannstoolsdataloggerreader;

/* Voltcraft.java (c) 2011-2023 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2023
 * =======================================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;

import android.os.Environment;
import android.util.Log;

public class Voltcraft {
  private static final String TAG = "VOLTCRAFT"; 
  private static final boolean dologpacket=true; // Bad Blocks rausschreiben


  /* Transmission Commands*/

  public final static int CMD_GETCONFIG=0x00;         // Read device configuration block.
  public final static int CMD_GETCONFIG_WEATHER=0x0F; // Read device block.
  public final static int CMD_READMEM=0x00;           // Read device block.
  public final static int CMD_READSINGLE=0xFF;        // Read single measurement (sound).
  public final static int CMD_READMEM_WEATHER=0x0F;   // Read device block.
  public final static int CMD_SETCONFIG=0x01;         // Write device configuration block.
  public final static int CMD_SETCONFIG_WEATHER=0x0e; // Write device configuration block.
  public final static int CMD_SETCONFIG_SOUND=0x0e;   // Write device configuration block.
  public final static int CMD_CALIBRATION=0x0c;       // Read and set calibration (sound)
  public final static int CMD_GETANZDATA=0x0d;        // Read number of stored data points (sound)


  public static int chk_conf(LoggerConfig config, int loggertype, int memory_size) {
    String n=config.getname();
    if(n.length()==0) return Error.ERR_CNONAME;
    if(n.length()>16) return Error.ERR_CNAME;
    if(loggertype==Logger.LTYP_THP) {
      if(config.num_data_conf*6>=memory_size || config.num_data_conf<0) return Error.ERR_NUMDATA;
    } else if(loggertype==Logger.LTYP_TH) {
      if(config.num_data_conf*4>=memory_size || config.num_data_conf<0) return Error.ERR_NUMDATA;
    } else {
      if(config.num_data_conf*2>=memory_size || config.num_data_conf<0) return Error.ERR_NUMDATA;
    }
    if(config.set_interval>86400 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    return Error.OK;
  }

  public static byte[] build_conf(LoggerConfig config, int loggertype, int protocol) {
    if(protocol==Logger.PROTO_VOLTCRAFT_WEATHER) return build_conf2(config,loggertype);
    return build_conf1(config,loggertype);
  }
  
  /* Standard Voltcraft konfiguration, 64 Bytes, TH oder S*/
  
  private static byte[] build_conf1(LoggerConfig config, int loggertype) {	
    ByteBuffer mes=ByteBuffer.allocate(64);
    mes.position(0);
    mes.order(ByteOrder.LITTLE_ENDIAN);
    if(loggertype==Logger.LTYP_SOUND) { /* DL160S and DL161S */
      mes.put(0x0,(byte)(config.led_conf & 0x1f)); /* Blink interval 0=none, 10, 20, 30.*/
      int a=0;
      /*
	4 	Bitfield representing the following flags:
      (0<<7) |	// 0/1: auto/manual 0=immediate, 1=manual
	(1<<6) |	// 0/1: realtime/store
	(0<<5) |	// 0/1: check?
	(1<<4) |	// 0/1: normal/peak 0=store measurements, 1=real-time
	(1<<3) |	// 0/1: dBC/dBA (frequency weighting) 0=dBC, 1=dBA
	(0<<2) |	// 0/1: slow/fast (time weighting) 0=Slow, 1=Fast
	(1<<0) ,	// 0/1/2/3: n.a. / sample rate in seconds / sample rate in minutes / sample rate in hours

	5 	Sample interval 1-7, representing 50ms, 500ms, 1s, 2s, 5s, 10s, 60s respectively.
	6 	Last two digits of year, e.g. 0x0d for 2013
	7 	Month, 1-12
	8 	Day of month, 1-31
	9 	Hours
	10 	Minutes
	11 	Seconds
	12-14 	Number of samples to acquire, as a big-endian integer.
	15 	Alarm low threshold
	16 	Alarm high threshold 
---> Evtl Unterschiede zwischen DL160S und DL-161S ???

      */
      if(config.startcondition==Logger.START_BUTTON) a+=128;
      mes.put(0x1,(byte)a);
      mes.put(0x2,(byte)config.set_interval); /* Sample interval 1-7, representing 50ms, 500ms, 1s, 2s, 5s, 10s, 60s respectively.*/
      mes.put(0x3,(byte)60); /* Alarm High level in dB */
      mes.put(0x4,(byte)30); /* Alarm Low level in dB */
      mes.put(0x5,(byte)(config.time_year-2000)); /*Last two digits of year*/
      mes.put(0x6,config.time_mon);
      mes.put(0x7,config.time_mday);
      mes.put(0x8,config.time_hour);
      mes.put(0x9,config.time_min);
      mes.put(0xa,config.time_sec);
      mes.put(0xb,(byte)((config.num_data_conf>>16)&0xff));
      mes.put(0xc,(byte)((config.num_data_conf>>8)&0xff));
      mes.put(0xd,(byte)((config.num_data_conf>>0)&0xff));
      mes.put(0x18,(byte)0x8c);
      mes.put(0x19,(byte)0x39);
      mes.put(0x1a,(byte)0xbb);
      mes.put(0x1b,(byte)0x78);
      mes.put(0x1c,(byte)0x03);

      mes.put(0x20,(byte)0x0e);
      mes.put(0x21,(byte)0x40);

    } else {
		mes.putInt(0,0xce); /*Config begin for write config*/
		mes.putInt(4,config.num_data_conf);
		mes.putInt(8,0);
		mes.putInt(12,config.set_interval);            	
		mes.putInt(16,config.time_year);
		mes.putShort(20,(short) 0);
		mes.putShort(22,Voltcraft.num2bin(config.templimit_low));
		mes.putShort(24,(short) 0);
		mes.putShort(26,Voltcraft.num2bin(config.templimit_high));
		mes.put(28,config.time_mon);
		mes.put(29,config.time_mday);
		mes.put(30,config.time_hour);
		mes.put(31,config.time_min);
		mes.put(32,config.time_sec);
		mes.put(33,(byte)config.temp_unit);            	
		mes.put(34,config.led_conf);
		String n=config.getname();
		mes.position(35);
		mes.put(n.getBytes());
		mes.putChar(35+n.length(),(char) 0);

		mes.put(51,config.start);
		mes.putShort(52,(short) 0);
		mes.putShort(54,Voltcraft.num2bin(config.humilimit_low));
		mes.putShort(56,(short) 0);
		mes.putShort(58,Voltcraft.num2bin(config.humilimit_high));

		mes.putInt(60,0xce);
    }
    return mes.array();
  }


	private static byte[] build_conf2(LoggerConfig config, int loggertype) {
		ByteBuffer mes=ByteBuffer.allocate(64);
		mes.position(0);
		mes.order(ByteOrder.BIG_ENDIAN);
		if(config.set_interval<60) {
		  Log.d(TAG,"Config WARNING: Sample rate set to 1 minute.");
		  config.set_interval=60;
		}    
		mes.put(0,(byte) config.time_sec);
		mes.put(1,(byte) config.time_min);
		mes.put(2,(byte) config.time_hour);
		mes.put(3,(byte) config.time_mday);
		mes.put(4,(byte) config.time_mon);    	
		mes.putShort(5,(short)config.time_year);
		mes.put(7,(byte) 0xff);
		mes.put(8,(byte) 0xff);
		mes.put(9,(byte) 0xff);
		mes.put(10,(byte) (((config.led_conf&0x7f)==0)?0xff:(config.led_conf&0x7f)));
		mes.put(11,(byte) (((config.led_conf&0x80)==0x80)?3:0xff)); /*3 Sek blink*/
		mes.putShort(0xc,(short) (config.set_interval/60));
		mes.put(0xe,(byte) ((config.start==2)?1:0));
		mes.putShort(0xf,(short) (config.templimit_high*100));
		//    	mes.put(0xf,(byte) 0x1b);  // 7000 Schwelle high
		//    	mes.put(0x10,(byte) 0x58); //
		mes.putShort(0x11,(short) (config.templimit_low*100));
		//    	mes.put(0x11,(byte) 0xf0); // -4000 Schwelle low
		//    	mes.put(0x12,(byte) 0x60); //
		mes.putShort(0x13,(short) (config.humilimit_high*10));
		//   	mes.put(0x13,(byte) 0x03); // 1000
		//    	mes.put(0x14,(byte) 0xe8); //
		mes.putShort(0x15,(short) (config.humilimit_low*10));
		//   	mes.put(0x15,(byte) 0x00); // 0
		//    	mes.put(0x16,(byte) 0x00); //
		mes.putShort(0x17,Voltcraft.p2raw((double)config.preslimit_high));
		//    	mes.put(0x17,(byte) 0x03); // 868
		//    	mes.put(0x18,(byte) 0x64); //
		mes.putShort(0x19,Voltcraft.p2raw((double)config.preslimit_low));
		//    	mes.put(0x19,(byte) 0xf3); // -3132 
		//    	mes.put(0x1a,(byte) 0xc4); //
		mes.put(0x1b,(byte) 0x5a); //  90  ???
		mes.putInt(0x1c,config.num_data_conf);	  	
		return mes.array();
	}

  public static int get_config(Logger logger) {
    if(logger.protocol==Logger.PROTO_VOLTCRAFT_NEW) return getconfig_voltcraft_new(logger);
    else if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) return getconfig_voltcraft_weather(logger);
    else return getconfig_voltcraft(logger);
  }
	private static void apply_offset(final LoggerConfig config) {
		/* apply time correction */

		Calendar cal = Calendar.getInstance();
		cal.set(config.time_year, (int)(config.time_mon&0xff)-1, (int)(config.time_mday&0xff), (int)(config.time_hour&0xff), 
				(int)(config.time_min&0xff), (int)(config.time_sec&0xff));
		long ss=cal.getTimeInMillis()/1000;
		ss+=config.time_delay;
		cal.setTimeInMillis(ss*1000);
		config.time_year=(cal.getTime().getYear()+1900);
		config.time_mon=(byte)(cal.getTime().getMonth()+1);
		config.time_mday=(byte)cal.getTime().getDate();
		config.time_hour=(byte)cal.getTime().getHours();
		config.time_min=(byte)cal.getTime().getMinutes();
		config.time_sec=(byte)cal.getTime().getSeconds();

	}

  private static int getconfig_voltcraft(Logger logger) {
    int r;
    int ret=Error.OK;
    logger.sendCommand((byte)CMD_GETCONFIG,(byte)16,(byte)1);
    if((r=logger.readresponse((byte)CMD_GETCONFIG,(byte)16,(byte)1))<0) return(Error.ERR);

    if(r==0) logger.updateMessage("OK. no data available",0);
    else logger.updateMessage("OK. "+r+" Bytes can be read r=0x"+String.format("%x",r),0);  

    final LoggerConfig config=logger.config;
    byte cbuf[]=new byte[logger.packet_size];

    //  	    ByteBuffer buffer = ByteBuffer.allocate(64);
    //  	    UsbRequest request = new UsbRequest();
    //  	    request.initialize(logger.Connection, logger.EndpointIn);
    //  	    request.queue(buffer, 64);  /* queue a request on the interrupt endpoint */
    // wait for status event
    ret=logger.receive(cbuf);
    if(ret>=64) {
			//	    	if(logger.Connection.requestWait() == request) {
			config.configbuf=cbuf.clone();
			ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
			buffer.position(0);
			buffer.put(cbuf);
			buffer.position(0);
			config.mglobal_message="";
			config.flag_bits=0;
			config.time_delay=0;

			buffer.order(ByteOrder.LITTLE_ENDIAN);
			config.config_begin=buffer.getInt(0);
			Log.d(TAG,"config_begin ="+buffer.getInt(0));
			config.num_data_conf=buffer.getInt(4);
			Log.d(TAG,"num_data_conf="+buffer.getInt(4));
			config.num_data_rec=buffer.getInt(8);
			Log.d(TAG,"num_data_rec ="+buffer.getInt(8));
			config.got_interval=buffer.getInt(12);
			config.set_interval=config.got_interval;
			Log.d(TAG,"interval     ="+buffer.getInt(12));
			config.time_year=buffer.getInt(16);
			Log.d(TAG,"time_year    ="+buffer.getInt(16));
			config.templimit_low=Voltcraft.bin2num(buffer.getShort(22));
			Log.d(TAG,"templimit_low ="+config.templimit_low);
			config.templimit_high=Voltcraft.bin2num(buffer.getShort(26));
			Log.d(TAG,"templimit_high ="+config.templimit_high);
			config.time_mon=buffer.get(28);  Log.d(TAG,"time_mon    ="+(int)(buffer.getChar(28) & 255));
			config.time_mday=buffer.get(29); Log.d(TAG,"time_mday    ="+(int)(buffer.getChar(29) & 255));
			config.time_hour=buffer.get(30); Log.d(TAG,"time_hour    ="+(int)(buffer.getChar(30) & 255));
			config.time_min=buffer.get(31);  Log.d(TAG,"time_min     ="+(int)(buffer.getChar(31) & 255));
			config.time_sec=buffer.get(32);  Log.d(TAG,"time_sec     ="+(int)(buffer.getChar(32) & 255));
			config.temp_unit=buffer.get(33);  Log.d(TAG,"temp_unit     ="+(int)(buffer.getChar(33) & 255));
			config.led_conf=buffer.get(34);  Log.d(TAG,"led_conf     ="+config.led_conf);
			int i;
			for(i=0;i<16;i++) {
				config.name[i]=(char)(buffer.getChar(35+i)&255);
				if(config.name[i]==0) break;
			}

			Log.d(TAG,"name     ="+config.getname());
			config.start=buffer.get(51);
			config.humilimit_low=Voltcraft.bin2num(buffer.getShort(54));
			config.humilimit_high=Voltcraft.bin2num(buffer.getShort(58));
			config.config_end=buffer.getInt(60);
			Log.d(TAG,"config_end ="+config.config_end);

			if(config.config_begin==config.config_end) {
				if(config.start==1 && config.config_begin!=0) {
					config.time_delay=buffer.getInt(0);           /*Delay in seconds since button pressed*/
					apply_offset(config);  /* apply time correction */
				} else config.time_delay=0;
				/* Find out which Logger type is connected: */
				if(r>0 && config.num_data_rec>0) {
					if(r/config.num_data_rec==2) logger.loggertype=Logger.LTYP_TEMP;
					else if(r/config.num_data_rec==4) logger.loggertype=Logger.LTYP_TH;
					else if(r/config.num_data_rec==6) logger.loggertype=Logger.LTYP_THP;
					else {
						config.mglobal_message+="WARNING: Config Irregularity: r="+r+":"+config.num_data_rec+
								". Maybe the battery needs to be replaced.";
						ret=Error.WARN_NOTSUP;  /*Notsupported/Not tested Message*/
					}
				}
			} else {
				ret=Error.ERR_BAD;
				config.mglobal_message+="ERROR: Config bad. ["+config.config_begin+"/"+config.config_end+"]";
				Log.e(TAG,config.mglobal_message);
			}
			logger.isreadconfig=true;
    } else return(Error.ERR);	
    return ret;
  }
	private static int getconfig_voltcraft_new(Logger logger) {
		int r;
		int ret=Error.OK;
		final LoggerConfig config=logger.config;
		logger.unlock_device(); // TODO
		logger.sendCommand((byte)CMD_GETCONFIG,(byte)16,(byte)1);
		if((r=logger.readresponse((byte)0x0,(byte)16,(byte)1))<0) return(Error.ERR);
		if(r==0) logger.updateMessage("OK. no data available",0);
		else  logger.updateMessage("OK. "+r+" Bytes can be read. r=0x"+String.format("%x",r),0);	

		byte cbuf[]=new byte[logger.packet_size];

		//	ByteBuffer buffer = ByteBuffer.allocate(64);
		//	UsbRequest request = new UsbRequest();
		//	request.initialize(logger.Connection, logger.EndpointIn);
		//	request.queue(buffer, 64);  /* queue a request on the interrupt endpoint */
		// wait for status event
		ret=logger.receive(cbuf);
		if(ret>=64) {
			// 	if(logger.Connection.requestWait() == request) {
			config.configbuf=cbuf.clone();
			ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
			buffer.position(0);
			buffer.put(cbuf);
			buffer.position(0);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			config.mglobal_message="";
			config.flag_bits=0;
			config.time_delay=0;
			config.config_begin=buffer.getInt(0);
			Log.d(TAG,"config_begin ="+buffer.getInt(0));
			config.num_data_conf=buffer.getInt(4);
			config.num_data_rec=buffer.getInt(8);
			config.got_interval=buffer.getInt(12);
			config.set_interval=config.got_interval;
			config.time_year=buffer.getInt(16);
			config.templimit_low=Voltcraft.bin2num(buffer.getShort(22));
			Log.d(TAG,"templimit_low ="+config.templimit_low);
			config.templimit_high=Voltcraft.bin2num(buffer.getShort(26));
			Log.d(TAG,"templimit_high ="+config.templimit_high);
			config.time_mon=buffer.get(28);  Log.d(TAG,"time_mon    ="+(int)(buffer.getChar(28) & 255));
			config.time_mday=buffer.get(29); Log.d(TAG,"time_mday    ="+(int)(buffer.getChar(29) & 255));
			config.time_hour=buffer.get(30); Log.d(TAG,"time_hour    ="+(int)(buffer.getChar(30) & 255));
			config.time_min=buffer.get(31);  Log.d(TAG,"time_min     ="+(int)(buffer.getChar(31) & 255));
			config.time_sec=buffer.get(32);  Log.d(TAG,"time_sec     ="+(int)(buffer.getChar(32) & 255));
			config.temp_unit=buffer.get(33);  
			Log.d(TAG,"temp_unit     ="+(int)(buffer.getChar(33) & 255));
			config.led_conf=buffer.get(34);  Log.d(TAG,"led_conf     ="+config.led_conf);
			int i;
			for(i=0;i<16;i++) {
				config.name[i]=(char)(buffer.getChar(35+i)&255);
				if(config.name[i]==0) break;
			}
			Log.d(TAG,"name     ="+config.getname());
			config.start=buffer.get(51);  Log.d(TAG,"start     ="+config.start);
			config.humilimit_low=Voltcraft.bin2num(buffer.getShort(54));
			Log.d(TAG,"humilimit_low ="+config.humilimit_low);
			config.humilimit_high=Voltcraft.bin2num(buffer.getShort(58));
			Log.d(TAG,"humilimit_high ="+config.humilimit_high);
			config.config_end=buffer.getInt(60);
			Log.d(TAG,"config_end ="+config.config_end);
			if(config.config_begin==config.config_end) {
				if(config.start==1 && config.config_begin!=0) {
					config.time_delay=buffer.getInt(0);           /*Delay in seconds since button pressed*/
					apply_offset(config);
				} else config.time_delay=0;
				/* Find out which Logger type is connected: */
				if(r>0 && config.num_data_rec>0) {

					if(r/config.num_data_rec==2) logger.loggertype=Logger.LTYP_TEMP;
					else if(r/config.num_data_rec==4) logger.loggertype=Logger.LTYP_TH;
					else if(r/config.num_data_rec==6) logger.loggertype=Logger.LTYP_THP;
					else {
						config.mglobal_message+="WARNING: Config Irregularity: r="+r+":"+config.num_data_rec+
								". Maybe the battery needs to be replaced.";
						ret=Error.WARN_NOTSUP;  /*Notsupported/Not tested Message*/
					}
				}
			} else {
				ret=Error.ERR_BAD;
				config.mglobal_message+="ERROR: Config bad. ["+config.config_begin+"/"+config.config_end+"]";
				Log.e(TAG,config.mglobal_message);
			}
			logger.isreadconfig=true;
		} else return(Error.ERR);
		logger.lock_device(); // TODO
		return ret;
	}
	private static int getconfig_voltcraft_weather(Logger logger) {
		int r;
		int ret=Error.OK;
		logger.unlock_device();
		logger.sendCommand((byte)CMD_GETCONFIG_WEATHER,(byte)0,(byte)0);
		if((r=logger.readresponse((byte)CMD_GETCONFIG_WEATHER,(byte)0,(byte)0))<0) {
			logger.lock_device();
			return(Error.ERR);
		}
		final LoggerConfig config=logger.config;
		if(r==0) logger.updateMessage("OK. No data available.",0);
		else  logger.updateMessage("OK. "+r/6+" Datapoints can be read. r=0x"+String.format("%x",r)+" "+r+" Bytes.",0);

		byte cbuf[]=new byte[logger.packet_size];

		//	UsbRequest request = new UsbRequest();
		//	request.initialize(logger.Connection, logger.EndpointIn);
		//	request.queue(buffer, 32); /* queue a request on the interrupt endpoint */
		ret=logger.receive(cbuf,32);
		if(ret>=32) {
			config.configbuf=cbuf.clone();
			// wait for status event
			//   	if(logger.Connection.requestWait() == request) {
			ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
			buffer.position(0);
			buffer.put(cbuf);
			buffer.position(0);
			buffer.order(ByteOrder.BIG_ENDIAN);

			config.config_begin=0;
			config.config_end=0;
			config.mglobal_message="";
			config.flag_bits=0;
			config.time_delay=0;
			config.temp_unit=Logger.UNIT_C;
			config.setname(logger.Serial_id);
			config.num_data_rec=buffer.getInt(0x1c);
			if(config.num_data_conf>10800) config.num_data_conf=10800;
			config.got_interval=buffer.getShort(0xc)*60;
			config.set_interval=config.got_interval;
			config.time_year=buffer.getShort(5);
			config.time_mon=buffer.get(4);
			config.time_mday=buffer.get(3);
			config.time_hour=buffer.get(2);
			config.time_min=buffer.get(1);
			config.time_sec=buffer.get(0);
			config.start=(byte)(buffer.get(0xe)+1);
			config.led_conf=(byte) ((buffer.get(0xa)==0xff)?0:(buffer.get(0xa)&0x7f));
			if(buffer.get(0xb)!=0xff) config.led_conf|=0x80;
			config.humilimit_low=(float) (buffer.getShort(0x15)/10.0);
			config.humilimit_high=(float) (buffer.getShort(0x13)/10.0);
			config.preslimit_low= Voltcraft.raw2p(buffer.getShort(0x19));
			config.preslimit_high= Voltcraft.raw2p(buffer.getShort(0x17));
			config.templimit_low=(float)(buffer.getShort(0x11)/100.0);
			config.templimit_high=(float)(buffer.getShort(0xf)/100.0);
			logger.loggertype=Logger.LTYP_THP;
			if(r!=6*config.num_data_rec) {
				config.mglobal_message+="WARNING: Config Irregularity: r="+r+":"+config.num_data_rec+
						". Maybe the battery needs to be replaced.";
				// ret=WARN_NOTSUP;  /*Notsupported/Not tested Message*/
			}
			logger.isreadconfig=true;
			logger.lock_device();
			logger.reset();
			logger.unlock_device();
		} else ret=Error.ERR;	
		logger.lock_device();
		return(ret);
	}
	public static int get_data(Logger logger) {
		int q=0,count=0,retry=5;
		logger.data.quality=0;
		if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER || logger.protocol==Logger.PROTO_VOLTCRAFT_NEW) logger.unlock_device();
		int i=logger.config.num_data_rec;
		while(i>0) {
			q=getmembank(logger,count);
			if(q<0) {
				Log.d(TAG,"Error getting membank "+ q);
				break;
			}
			if(q>0 && retry>0) {
				retry--;
				Log.d(TAG,"transmission error. Retry... ("+retry+")");
			} else {
				logger.data.quality+=q;
				i=logger.config.num_data_rec-logger.data.anzdata;
				count++;
				retry=5;
			}
		}
		if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER || logger.protocol==Logger.PROTO_VOLTCRAFT_NEW) logger.lock_device();
		return(q);
	}
	
	
  private static int sendCommandResponse(Logger logger,int control) {
    byte a,b,c;
    if(control==-1) {  /* Read config */
      if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) {a=(byte)CMD_GETCONFIG_WEATHER;b=(byte)0;c=(byte)0;}
      else {a=(byte)CMD_GETCONFIG;b=(byte)16;c=(byte)1;}
    } else if(control==-2) {  /* Write config */
      if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) a=(byte)CMD_SETCONFIG_WEATHER;
      else {
        if(logger.loggertype==Logger.LTYP_SOUND) a=(byte)CMD_SETCONFIG_SOUND;
	else a=(byte)CMD_SETCONFIG;
      }
      b=(byte)0x40;
      c=(byte)0;
    } else {		/* Read data bank */
      if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) {a=(byte)CMD_READMEM_WEATHER;b=(byte)(1+control);c=(byte)0;}
      else {a=(byte)CMD_READMEM;b=(byte)(control&0xf);c=(byte)0x40;}
    }
    logger.sendCommand(a,b,c);
    return logger.readresponse(a,b,c);
  }
	
	
	
	/*Read a memory bank from device. A Memory-bank consists of 
	 * 64 packages of 64 Bytes, or 64 times 16 Datapoints
	 * */
	public static int getmembank(Logger logger,int adr) {
		int i,q=0;
		int sysstatus=0;
		if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) {
			if(adr<0 || adr>0x80) return Error.ERR;	
		} else {
			if(adr<0 || adr>15) return Error.ERR;
		}
		final LoggerData data=logger.data;
		Log.d(TAG,"Getmembank "+adr);
		
		
		
		sysstatus=sendCommandResponse(logger,adr);
		
		if(sysstatus<0) return(-2);
		if(logger.loggertype==Logger.LTYP_THP) data.anzdata=adr*85;
		else if(logger.loggertype==Logger.LTYP_TH) data.anzdata=adr*1024;
		else data.anzdata=adr*1024*2;
		logger.updateStatus(sysstatus);

		if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER) {
			if(getdatapacket2(logger,logger.config.num_data_rec-data.anzdata)<0) {
				logger.updateStatus(Error.ERR);
			} else {
				if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
					for(i=adr*85;i<data.anzdata;i++) {
						if(i>=logger.config.num_data_rec) break;
						if(data.get_temp(i)>300 || data.get_temp(i)<-100 || 
								data.get_rh(i)>104 || data.get_rh(i)<-1) q++;    		
					}
				}
				// Update the progress bar
				logger.updateProgress((int)(data.anzdata*100.0/logger.config.num_data_rec));
			}
		} else {
			for(i=0;i<4096/logger.packet_size;i++) {
				/* read response data (64 byte) */
				if(getdatapacket(logger)<0) {
					logger.updateStatus(Error.ERR);
				} else {
					// Update the progress bar
					logger.updateProgress((int)(data.anzdata*100.0/logger.config.num_data_rec));
				}
			}

			/*Nun Qualitaetscheck der Daten machen.*/
			if(logger.loggertype==Logger.LTYP_TH) {
				for(i=adr*1024;i<(adr+1)*1024;i++) {
					if(i>=logger.config.num_data_rec) break;
					if(data.get_temp(i)>300 || data.get_temp(i)<-100 || 
							data.get_rh(i)>104 || data.get_rh(i)<-1) q++;    		
				}
			} else if(logger.loggertype==Logger.LTYP_TEMP) {
				for(i=adr*1024*2;i<(adr+1)*1024*2;i++) {
					if(i>=logger.config.num_data_rec) break;
					if(data.temp[i]>300*10 || data.temp[i]<-100*10) q++;
				}
			}
		}
		Log.d(TAG,"read membank #"+adr+" Quality: "+q);

		if(logger.protocol==Logger.PROTO_VOLTCRAFT_NEW) { 
			if(logger.readack()!=1) Log.d(TAG,"no ACK in response after membank #"+adr);
			//    	  msysstatus=readresponse((byte)0x0,(byte)(adr&0xf),(byte)0x40); /* I dont know why this is necessary ????*/
			//    	  Log.d(TAG,"response after membank #"+adr+": "+msysstatus);
		}
		return(q);
	}

	/*Read one datapacket */
	private static int getdatapacket(final Logger logger) {
		byte buf[]=new byte[logger.packet_size];
		final LoggerData data=logger.data;
		int ret=logger.receive(buf);
		if(ret==0) {
			Log.d(TAG,"ret=0, Retry...");
			ret=logger.receive(buf);
		}
		if(ret==logger.packet_size) {
			int i;
			if(logger.loggertype==Logger.LTYP_THP) {
				for(i=0;i<ret/6;i++) {
					data.add((short) ((short)buf[i*6+2]&0xff | ((short)buf[i*6+3]&0xff)<<8)
							,(short) ((short)buf[i*6+4]&0xff | ((short)buf[i*6+5]&0xff)<<8)
							,(short) ((short)buf[i*6]&0xff | ((short)buf[i*6+1]&0xff)<<8));
				}
			} else if(logger.loggertype==Logger.LTYP_TH) {
				for(i=0;i<16;i++) {
					data.add((short) ((short)buf[i*4]&0xff | ((short)buf[i*4+1]&0xff)<<8),
							(short) ((short)buf[i*4+2]&0xff | ((short)buf[i*4+3]&0xff)<<8));
				}
			} else {
				for(i=0;i<32;i++) 
					data.add((short) ((short)buf[i*2]&0xff | ((short)buf[i*2+1]&0xff)<<8));
			}
			if(dologpacket) {
				int q=0;
				/*Nun schnellen Qualitaetscheck der Daten machen.*/
				if(logger.loggertype==Logger.LTYP_TH) {
					for(i=data.anzdata-16;i<data.anzdata;i++) {
						if(data.get_temp(i)>300 || data.get_temp(i)<-100 || 
								data.get_rh(i)>104 || data.get_rh(i)<-1) q++;    		
					}
				} else if(logger.loggertype==Logger.LTYP_TEMP) {
					for(i=data.anzdata-16;i<data.anzdata;i++) {
						if(data.temp[i]>300*10 || data.temp[i]<-100*10) q++;
					}
				}
				if(q>0) logpacket(buf,data.anzdata);
			}
			Log.d(TAG,"Got Datapacket ("+logger.packet_size+" bytes): OK. "+data.anzdata);
			return Error.OK;
		}
		else return Error.ERR;  /*Something is wrong: timeout*/
	}

	private static void logpacket(final byte buf[], int idx) {
		String filename="bad-blocks.hex";
		boolean append=true;
		FileOutputStream fos=null;
		OutputStreamWriter osw=null;

		try {
			File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			dirdata.mkdirs();
			File file=new File(dirdata,filename);
			fos=new FileOutputStream(file,append);
			osw=new OutputStreamWriter(fos);
			osw.write("\n# Bad Block detected idx="+idx+"  len="+buf.length+"\n");
			String line="";
			for(int i=0;i<buf.length;i++) {
				line+=String.format("%02x ",buf[i]);
				if(((i+1)%16)==0) {
					osw.write(line+"\n");
					line="";
				}
			}
			osw.write(line+"\n");

		} catch (Throwable thr) {
			// FilenotFound oder IOException
			Log.e(TAG,"open/save. ERROR ",thr);
		} finally {
			if(osw!=null) {try {osw.close();} catch (IOException e) {Log.e(TAG,"osw.close ",e);}}
			if(fos!=null) {try {fos.close();} catch (IOException e) {Log.e(TAG,"fos.close ",e);}}

		}
	}

	/*Read one datapacket with 85 Values, protocol=2 weatherdatalogger*/
	private static int getdatapacket2(final Logger logger,int n) {
		int bbig;
		if(n>85) bbig=510;
		else bbig=n*6;
		if(bbig<0) return Error.ERR;
		byte buf[]=new byte[bbig];
		final LoggerData data=logger.data;
		int ret=logger.receive(buf);
		if(ret>0) {
			int i;
			if(logger.loggertype==Logger.LTYP_THP) {
				for(i=0;i<ret/6;i++) {
					data.add((short) ((short)buf[i*6+3]&0xff | ((short)buf[i*6+2]&0xff)<<8),
							(short) ((short)buf[i*6+5]&0xff | ((short)buf[i*6+4]&0xff)<<8),
							(short) ((short)buf[i*6+1]&0xff | ((short)buf[i*6]&0xff)<<8));
				}
			} else if(logger.loggertype==Logger.LTYP_TH) {
				for(i=0;i<ret/4;i++) {
					data.add((short) ((short)buf[i*4]&0xff | ((short)buf[i*4+1]&0xff)<<8),
							(short) ((short)buf[i*4+2]&0xff | ((short)buf[i*4+3]&0xff)<<8));
				}
			} else if(logger.loggertype==Logger.LTYP_TEMP) {
				for(i=0;i<ret/2;i++) data.add((short) ((short)buf[i*2]&0xff | ((short)buf[i*2+1]&0xff)<<8));
			}
			Log.d(TAG,"Got Datapacket ("+bbig+" bytes): OK. "+data.anzdata);
			return Error.OK;
		} else return Error.ERR;  /*Something is wrong: timeout*/
	}

  /* This is supposed to work with the SOUND loggers, but maybe also with others...*/

   private int read_single_measurement(Logger logger) {
     logger.sendCommand((byte)CMD_READSINGLE,(byte)0,(byte)0);
     byte buf[]=new byte[64];
     int ret=logger.receive(buf);
     int val=0;
     if(ret<0) ; /* ERROR*/
     else if(ret==2)  val=(int)buf[0]+256*(int)buf[1]; /* Success */
     else ; /* ??? */
     return(val);
   }



/* The calibration value is a signed int 
   in 0.1 dB steps, 
   0 is 0.0 dB, range according to manual: +/- 12.0 dB
   The calibration value is a relative value which is added to the current 
   calibration value stored in the device
   the accumulated value is returned from the device
   i.e. sending a 0.0 retrieves the stored value without altering the 
   calibration. */

  public final static int INVALID_CALIBRATION_VALUE=127;
  
  private float sound_calibration(Logger logger,float cal) {
    logger.sendCommand((byte)CMD_CALIBRATION,(byte)(int)(cal*10),(byte)0);
    float accval=0;
    byte buf[]=new byte[3];
    int ret=logger.receive(buf);
    if(ret==1) accval=(float)(((int)buf[0]))*(float)0.1;
    else accval=(float)INVALID_CALIBRATION_VALUE*(float)0.1;
    return(accval);
  }

  /* Get stored measurement count */
  private int get_anzdata(Logger logger) {
    logger.sendCommand((byte)CMD_GETANZDATA,(byte)0,(byte)0);
    int anzval=0;
    byte buf[]=new byte[3];
    int ret=logger.receive(buf);
    if(ret==2) anzval=(int)buf[0]+(int)buf[1]*256;
    else if(ret==2) anzval=(int)buf[0]+(int)buf[1]*256+(int)buf[2]*256*256;
    return(anzval);
  }
	
  public static String get_status(Logger logger) {
    String ret=""; 
    if(logger.isreadconfig) {
      ret=logger.typestring()+": typ="+logger.loggertype;
      if(logger.config.config_begin==0) ret=ret+": normal,";
      else ret=ret+": manual/delayed started, ("+logger.config.config_begin+"),";
      if(logger.config.num_data_rec==0) ret=ret+" NO DATA";
      else ret=ret+" DATA available ("+logger.config.num_data_rec+")";
    } else ret="Config has not yet been read";
    return ret+".";
  }

  /* Diese Umrechnungsfunktionen werden für die Schwellen 
     der Voltcraft-Logger gebraucht. */
  public static int bin2num(short a) {
    int ret=4711;
    boolean f=(a<0);
    a&=0x7fff;
    //      Log.d(TAG,"bintonum: "+a);
    if(a<0x3f80) ret=0;
    else if(a<=0x3f80) ret=1;
    else if(a<=0x4080) ret=((a-0x4000)>>6)+2;
    else if(a<=0x4100) ret=((a-0x4080)>>5)+4;
    else if(a<=0x4180) ret=((a-0x4100)>>4)+8;
    else if(a<=0x4200) ret=((a-0x4180)>>3)+16;
    else if(a<=0x4280) ret=((a-0x4200)>>2)+32;
    else if(a<=0x42C8) ret=((a-0x4280)>>1)+64;
    if(f) ret=-ret;
    return ret;
  }

  public static short num2bin(float af) {
    boolean f=false;
    short ret=0;
    int a = (int)af;
    if(a<0) {a=-a;f=true;}
    if(a==0) return 0;
    else if(a==1) ret=0x3f80;
    else if(a==2) ret=0x4000;
    else if(a<=4) ret= (short) ((short)0x4000+((a-2)<<6));
    else if(a<=8) ret= (short) ((short)0x4080+((a-4)<<5));
    else if(a<=16) ret= (short) ((short)0x4100+((a-8)<<4));
    else if(a<=32) ret= (short) ((short)0x4180+((a-16)<<3));
    else if(a<=64) ret= (short) ((short)0x4200+((a-32)<<2));
    else ret= (short) ((short)0x4280+((a-64)<<1));
    if(f) ret=(short) (ret&0x8000);
    return ret;
  } 
  /*This is the pressure calibration. Just a guess. Maybe we later want to have a settings
   * in the preferences....*/
  static float raw2p(short r) { return (float)1013.25+(float)r/10; }
  static short p2raw(double p) { return (short)((p-1013.25)*10); }
}
