Mit dieser App können Sie externe USB Datenlogger 
(für Temperatur, relative Luftfeuchtigkeit und/oder Luftdruck, Spannung, 
Strom, CO-Gas, Schallpegel) von Ihrem Tabletcomputer oder Smartphone auslesen, 
konfigurieren und eine neue Messung starten.

(Die App funktioniert nur auf Tabletcomputern oder Smartphones mit 
USB-(Host)-Verbindung/bzw. -Buchse. Android Versionen >3.1) 

Folgende Funktionen sind implementiert:

1. Konfiguration des Datenloggers auslesen,
2. Konfiguration des Datenloggers ändern und schreiben,
3. Messung starten und stoppen,
4. Meßdaten auslesen,
5. Meßdaten in Diagramm darstellen,
6. Meßdaten in Datei abspeichern, (.csv, .xml oder ASCII Format). 
7. Daten analysieren in einem interaktiven Diagramm oder in Tabellenform.

Folgende Datenlogger werden unterstützt:

* Voltcraft DL-120-TH, DL-100-T
* Voltcraft DL-121-TH, DL-101-T, DL-181-THP
* Voltcraft DL-141-TH (experimental)
* Voltcraft DL-200T, DL-210TH, DL-220THP, DL-230L, DL-240K (new)
* LOG32
* Lascar EL-USB-1,2,3,4
* USB500 Series
* FreeTec NC7004-675 black, KG100 (experimental)
* FreeTec NC7004-675 white, ecowitt DS102 (new)
* möglicherweise noch andere (nicht getestet).
